﻿Public Class OmaxControlHost
    Inherits Button
    Private Property HostedF As System.Windows.Forms.Panel
    <System.ComponentModel.Description("A control that Hosts other controls")> _
    <System.ComponentModel.Category("OmaxControl Properties")> _
    Public Property HostedControl As OmaxControl
        Get
            Return HostedF
        End Get
        Set(ByVal value As OmaxControl)
            HostedF = value
        End Set
    End Property

    Private Property TooltipTexts
    <System.ComponentModel.Description("Set tooltip to ControlHost")> _
<System.ComponentModel.Category("OmaxControl Properties")> _
    Public Property ToolTipText As String
        Get
            Return TooltipTexts
        End Get
        Set(ByVal value As String)
            TooltipTexts = value
            Me.OmaxToolTip.SetToolTip(Me, value)
        End Set
    End Property

    Private Sub HostControlDropDown(ByVal Sender As Object, ByVal e As EventArgs) Handles Me.Click
        Try
            If HostedF.Visible = True Then
                ClosePopUp()
            ElseIf HostedF.Visible = False Then
                ShowPopUp()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub HostLosctFocus(ByVal sender As Object, ByVal e As EventArgs) Handles Me.LostFocus
        'HostedF.Visible = False
        '   HostedF.Hide()
    End Sub
    Public Sub ShowPopUp()
        Try
            If (Me.Parent.Bounds.Bottom - Me.Location.Y) < HostedF.Height Then
                HostedF.Location = New Point(Me.Location.X, Me.Location.Y - HostedF.Height)
                HostedF.Show()
                HostedF.BringToFront()
                HostedF.Visible = True
            Else
                HostedF.Location = New Point(Me.Location.X, Me.Location.Y + Me.Height)
                HostedF.Show()
                HostedF.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Class OmaxControlHostToolTip
        Inherits ToolTip
        Private Property TooltipTexts
        <System.ComponentModel.Description("Set tooltip to ControlHost")> _
   <System.ComponentModel.Category("OmaxControl Properties")> _
        Public Property ToolTipText As String
            Get
                Return TooltipTexts
            End Get
            Set(ByVal value As String)
                TooltipTexts = value
            End Set
        End Property

    End Class
    Public Sub ClosePopUp()
        Try
            HostedF.Visible = False
            HostedF.Hide()
        Catch ex As Exception

        End Try
    End Sub

End Class
