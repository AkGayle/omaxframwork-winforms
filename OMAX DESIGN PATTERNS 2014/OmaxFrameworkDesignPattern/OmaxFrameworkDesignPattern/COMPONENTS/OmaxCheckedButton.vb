﻿Public Class OmaxCheckedButton
    Inherits Button

    Public Enum CheckOption
        Checked
        Unchecked
    End Enum
    Private Property CheckStates As CheckOption
    Private Property CheckedBackc As Color
    Private Property UncheckedBackc As Color
    Private Property CheckOptions As CheckState

    <System.ComponentModel.Description("Specifies the Checstate of OmaxCheckedButton")> _
    <System.ComponentModel.Category("OmaxCheckedButton Properties")> _
    Public Property CheckSate() As CheckState
        Get
            Return CheckStates
        End Get
        Set(ByVal Value As CheckState)
            CheckStates = Value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Description("Specifies the Backcolor of OmaxCheckedButton when it is Checked")> _
   <System.ComponentModel.Category("OmaxCheckedButton Properties")> _
    Public Property CheckBackColor As Color
        Get
            Return CheckedBackc
        End Get
        Set(ByVal value As Color)
            CheckedBackc = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Description("Specifies the Backcolor of OmaxCheckedButton when it is UnChecked")> _
 <System.ComponentModel.Category("OmaxCheckedButton Properties")> _
    Public Property UnCheckBackColor As Color
        Get
            Return UncheckedBackc
        End Get
        Set(ByVal value As Color)
            UncheckedBackc = value
            Me.Invalidate()
        End Set
    End Property

    Event CheckChanged(ByVal Sender As Object, ByVal e As CheckState)
    Private IsChecked As Boolean
    Private Sub CheckChange(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Click
        If IsChecked = False Then
            RaiseEvent CheckChanged(Me, CheckState.Unchecked)
            Me.BackColor = UncheckedBackc
            CheckSate = CheckState.Unchecked
            IsChecked = True
            Exit Sub
        ElseIf IsChecked = True Then
            RaiseEvent CheckChanged(Me, CheckState.Checked)
            Me.BackColor = CheckedBackc
            CheckSate = CheckState.Checked
            IsChecked = False
            Exit Sub
        End If

    End Sub
End Class
