﻿Imports System.Drawing.Printing
Partial Class OmaxGridview
    Private WithEvents OmaxPrintDocument As New PrintDocument
    Private OmaxPrintDialog As New PrintPreviewDialog

    ''' <summary>
    ''' Shows a print preview dialog of OmaxGrid contents
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowPrintPreview()
        OmaxPrintDialog.Document = OmaxPrintDocument
        OmaxPrintDialog.ShowDialog()
        BestFitAllRowsAndColumns()
    End Sub

    ''' <summary>
    ''' Prints OmaxGrid contents
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Print()
        OmaxPrintDialog.Document = OmaxPrintDocument
        OmaxPrintDialog.ShowDialog()
        BestFitAllRowsAndColumns()
    End Sub


    Dim PageHeightReached As Integer
    Private Sub OmaxPrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles OmaxPrintDocument.PrintPage
        BestFitAllRowsAndColumnsForPrint()

        Dim leftMargin As Integer = 60
        Dim position As Integer = leftMargin
        Dim yPosition As Integer
        Dim height As Integer = Me.ColumnHeadersHeight + 1
        yPosition = 0

        position = leftMargin
        yPosition = Me.ColumnHeadersHeight + 1
        For Each dr As DataGridViewColumn In Me.Columns
            Dim totalWidth As Double = dr.Width
            e.Graphics.FillRectangle(New SolidBrush(Color.LightGray), New Rectangle(position, yPosition, totalWidth, height))
            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(position, yPosition, totalWidth, height))
            e.Graphics.DrawString(dr.HeaderText, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, position, yPosition)
            position = position + totalWidth
        Next

        For Each dr As DataGridViewRow In Me.Rows
            position = leftMargin
            yPosition = yPosition + Me.ColumnHeadersHeight + 1
            For Each dc As DataGridViewCell In dr.Cells

                PageHeightReached += Rows(0).Height
                If PageHeightReached >= e.MarginBounds.Bottom - 10 Then
                    ' e.HasMorePages = True
                End If

                Dim totalWidth As Double = dc.OwningColumn.Width
                e.Graphics.FillRectangle(New SolidBrush(Color.White), New Rectangle(position, yPosition, totalWidth, height))
                e.Graphics.DrawRectangle(Pens.Black, New Rectangle(position, yPosition, dc.OwningColumn.Width, height))
                e.Graphics.DrawString(dc.Value, New Font("Arial", 12, FontStyle.Regular), Brushes.Black, position, yPosition)
                position = position + totalWidth
            Next
        Next
    End Sub

End Class

