﻿Public Class OmaxAnimatedInfoPanel
    Inherits Panel
    Private AniMatedBox As New PictureBox
    Friend WithEvents TitleLabel As New Label
    Friend WithEvents Descriptionlabel As New Label

    Sub NewP()
        TitleLabel.AutoSize = False
        Descriptionlabel.AutoSize = False
        Controls.Add(AniMatedBox)
        AniMatedBox.Dock = DockStyle.Left

        Controls.Add(TitleLabel)
        TitleLabel.Dock = DockStyle.Top
        Controls.Add(Descriptionlabel)
        Descriptionlabel.Dock = DockStyle.Fill
        AniMatedBox.SizeMode = PictureBoxSizeMode.Zoom
        AniMatedBox.Image = My.Resources.OmaxLoading
        AniMatedBox.BackColor = Color.Transparent
        Descriptionlabel.BringToFront()
        AniMatedBox.SendToBack()
    End Sub

    Private Property DescriptionFont_ As Font
    Private Property TitleFont_ As Font
    Private Property TitleText_ As String
    Private Property DescriptionText_ As String
    Private Property TitleColor As Color
    Private DescriptionColor As Color

    <System.ComponentModel.Description("Font style for text")> _
    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
    Public Property DescriptionFont() As Font
        Get
            Return DescriptionFont_
        End Get
        Set(ByVal value As Font)
            DescriptionFont_ = value
            Descriptionlabel.Font = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Description("Font style for text")> _
    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
    Public Property TitleFont() As Font
        Get
            Return TitleFont_
        End Get
        Set(ByVal value As Font)
            TitleFont_ = value
            TitleLabel.Font = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Description("Text to show in the title")> _
    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
    Public Property Titletext() As String
        Get
            Return TitleText_
        End Get
        Set(ByVal value As String)
            TitleText_ = value
            TitleLabel.Text = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Description("Text to show in the description")> _
    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
    Public Property DescriptionText() As String
        Get
            Return DescriptionText_
        End Get
        Set(ByVal value As String)
            DescriptionText_ = value
            Descriptionlabel.Text = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
     Public Property DescriptionForColor As Color
        Get
            Return DescriptionColor
        End Get
        Set(ByVal value As Color)
            DescriptionColor = value
            Descriptionlabel.ForeColor = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.Category("OmaxAnimatedPanel Properties")> _
     Public Property TiteForecolor As Color
        Get
            Return TitleColor
        End Get
        Set(ByVal value As Color)
            TitleLabel.ForeColor = value
            TitleColor = value
            Me.Invalidate()
        End Set
    End Property
End Class
