﻿Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxImage
Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxData

<System.ComponentModel.Description("OmaxImageSlider is an Imaging tool used to display images in a sliding manner")> _
Public Class OmaxPhotoSlider
    Inherits Windows.Forms.PictureBox
#Region "Properties"
    Private Property AutoSlide As Boolean
    Private Property Loops As Boolean
    Private Property SlideInterval As Integer
    Private Images_ As ImageList

    <System.ComponentModel.Category("ImageSlider Properties")> _
    <System.ComponentModel.Description("Indicates if the images in ImageSlider Auto scroll")> _
    Public Property EnableAutoSlide() As Boolean
        Get
            Return AutoSlide
        End Get
        Set(ByVal value As Boolean)
            AutoSlide = value
            Me.Invalidate()
        End Set
    End Property
    <System.ComponentModel.Category("ImageSlider Properties")> _
    <System.ComponentModel.Description("The frequency at which scrolling happens")> _
    Public Property Interval() As Integer
        Get
            Return SlideInterval
        End Get
        Set(ByVal value As Integer)
            SlideInterval = value
            Me.Invalidate()

        End Set
    End Property

    <System.ComponentModel.Category("ImageSlider Properties")> _
   <System.ComponentModel.Description("Indicates if images should loop")> _
    Public Property LoopImages As Boolean
        Get
            Return Loops
        End Get
        Set(ByVal value As Boolean)
            Loops = value
        End Set
    End Property


    Private Property Dsource As String
    Private Property DataBaseTypes As DataBaseType
    Private Property ConnectionStrings As String
    Private Property Cmd As String

    <System.ComponentModel.Category("ImageSlider Properties")> _
 <System.ComponentModel.Description("DataSource for images as string")> _
    Public Property DataSource As String
        Get
            Return Dsource
        End Get
        Set(ByVal value As String)
            Dsource = value
        End Set
    End Property

    <System.ComponentModel.Category("ImageSlider Properties")> _
<System.ComponentModel.Description("The type of database you want to connect to")> _
    Public Property DataBaseType() As DataBaseType
        Get
            Return DataBaseTypes
        End Get
        Set(ByVal value As DataBaseType)
            DataBaseTypes = value
        End Set
    End Property

    <System.ComponentModel.Category("ImageSlider Properties")> _
<System.ComponentModel.Description("The ConnectionString for the database you want to Connect to")> _
    Public Property ConnectionString() As String
        Get
            Return ConnectionStrings
        End Get
        Set(ByVal value As String)
            ConnectionStrings = value
        End Set
    End Property

    <System.ComponentModel.Category("ImageSlider Properties")> _
<System.ComponentModel.Description("Command for the Query to get images from your database")> _
    Public Property Command() As String
        Get
            Return Cmd
        End Get
        Set(ByVal value As String)
            Cmd = value
        End Set
    End Property

    <System.ComponentModel.Category("ImageSlider Properties")> _
<System.ComponentModel.Description("A list of images to Display in Image Slider")> _
    Public Property Images() As ImageList
        Get
            Return Images_
        End Get
        Set(ByVal value As ImageList)
            Images_ = value
            OmaxImages = value
        End Set
    End Property

#End Region

    Partial Class ImageCollection
        Inherits OmaxImageList
    End Class


#Region "Triggers"
    Private ImageCount, TotalImages, currentImageCount As Integer
    Dim I As Integer
    Private IsLoaded As Boolean
    Private Sub OmaxTimer1(sender As Object, e As EventArgs) Handles OmaxTimer.Tick
        If IsNothing(Image) = True Then
            Try
                Dim CURRENTIMAGE As DataRowView = OmaxImageSource.Current
                TotalImages = OmaxImageSource.Count
                If IsNothing(CURRENTIMAGE.Item(0)) = False Then
                    Image = Image.FromStream(GetPicture(CURRENTIMAGE.Item(0)))
                End If
                IsLoaded = True
            Catch ex As Exception

            End Try
        End If

        If Interval <= 0 Then
            Interval = 10
        End If


        If AutoSlide = True AndAlso ImageCount > 0 Then
            I += 1
            Try
                If I > Interval Then
                    currentImageCount += 1
                    If currentImageCount = ImageCount Then
                        If Loops = True Then
                            OmaxImageSource.MoveFirst()
                            currentImageCount = 0
                        End If
                    Else
                        OmaxImageSource.MoveNext()
                    End If
                    Dim CURRENTIMAGE As DataRowView = OmaxImageSource.Current
                    If IsNothing(CURRENTIMAGE.Item(0)) = False Then
                        Image = Image.FromStream(GetPicture(CURRENTIMAGE.Item(0)))
                    End If

                    I = 0

                End If
            Catch ex As Exception

            End Try

        End If
    End Sub

    ''' <summary>
    ''' Get images from the database by the Query supplied in Command
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadSliderImages()
        Try
            If Me.ConnectionString <> String.Empty Then

                If Command <> "Default" Then
                    OmaxImageSource.DataSource = HandleQuery(Me.Command, Me.ConnectionString, Me.DataBaseType, QueryType.ScalarQuery)
                    Dim ImageRow As DataRowView
                    For Each ImageRow In OmaxImageSource.List
                        ImageCount += 1
                        'If IsNothing(ImageRow.Item(0)) = False Then
                        '    AddPhoto(Image.FromStream(GetPicture(ImageRow.Item(0))), New System.Windows.Forms.PictureBox)
                        'End If
                    Next
                End If

            Else
                ' MsgBox("ConnectionStirng cannot be empty")
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Shared Property ImagesCount As Integer
    ''' <summary>
    ''' Slides to the Next Image
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SlideNext()
        Try
            OmaxImageSource.MoveNext()
            Dim CURRENTIMAGE As DataRowView = OmaxImageSource.Current
            If IsNothing(CURRENTIMAGE.Item(0)) = False Then
                Image = Image.FromStream(GetPicture(CURRENTIMAGE.Item(0)))
            End If
            StopSlide()
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Sledes to the previous Image
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SlidePrevious()
        Try
            OmaxImageSource.MovePrevious()
            Dim CURRENTIMAGE As DataRowView = OmaxImageSource.Current
            If IsNothing(CURRENTIMAGE.Item(0)) = False Then
                Image = Image.FromStream(GetPicture(CURRENTIMAGE.Item(0)))
            End If
            StopSlide()
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Stops the slide
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StopSlide()
        Try
            OmaxTimer.Enabled = False
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Starts The slide
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StartSlide()
        OmaxTimer.Enabled = True
    End Sub


    Private Shared ImageListControl As New List(Of System.Windows.Forms.PictureBox)
    Private Sub AddPhoto(ByVal Photo As Image, ByVal PhotoContainer As System.Windows.Forms.PictureBox)
        PhotoContainer.Image = Photo
        PhotoContainer.SizeMode = PictureBoxSizeMode.Zoom
        PhotoContainer.Size = New Point(50, 50)
        ImageListControl.Add(PhotoContainer)
    End Sub


#End Region
End Class
