﻿Public Class OmaxControl
    Inherits Panel
    Friend WithEvents PanelFooter As New Panel
    Friend WithEvents FooterCloseButton As New Button
    Private Sub LostF(ByVal sender As Object, ByVal e As EventArgs) Handles Me.LostFocus
        Me.Hide()
    End Sub
    Private Sub FooterbunttonClick(ByVal sender As Object, ByVal e As EventArgs) Handles FooterCloseButton.Click
        Me.Hide()
    End Sub
  

    Sub New1()
        Me.Hide()
        Me.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(PanelFooter)
        PanelFooter.Dock = DockStyle.Bottom
        PanelFooter.Height = 25
        PanelFooter.Controls.Add(FooterCloseButton)
        FooterCloseButton.Dock = DockStyle.Right
        FooterCloseButton.Width = 30
        FooterCloseButton.Text = "x"
        FooterCloseButton.ForeColor = Color.Red
        FooterCloseButton.Font = New Font("Arial", 12, FontStyle.Bold)
        FooterCloseButton.FlatStyle = FlatStyle.Flat
        FooterCloseButton.BackColor = Color.Transparent
        FooterCloseButton.FlatAppearance.BorderSize = 0

    End Sub


End Class
