﻿Public Class OmaxTextEdit
    Inherits TextBox
   
    Private EditValues_ As String
    Private EditFormats As String
    Private Null_ As String

    <System.ComponentModel.Description("The text you want to be displayed when the control is Null")> _
    <System.ComponentModel.Category("OmaxTextEdit Properties")> _
    Public Property NullValue As String
        Get
            Return Null_
        End Get
        Set(ByVal value As String)
            Null_ = value
            If Me.Text = vbNullString Then
                If value <> String.Empty Then
                    Me.Text = value
                    Me.Invalidate()
                End If

            End If
        End Set
    End Property

    Private Sub NullTextEnter() Handles Me.Enter
        Try
            If Me.Text = NullValue Then
                Me.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub NullTextLeave() Handles Me.Leave
        Try
            If Me.Text = vbNullString Then
                Me.Text = NullValue
            End If
        Catch ex As Exception

        End Try
    End Sub


    Sub new1()
        BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        Multiline = True
        Size = New Point(151, 24)

    End Sub


    Function EditValue()
        Return Val(Text.Replace("$", "").Replace(",", ""))
    End Function

   
End Class
