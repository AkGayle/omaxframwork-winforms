﻿Public Class OmaxGridview
    Inherits DataGridView
    Friend WithEvents GridSearch As New TextBox

    Sub new1()
        'BackgroundColor = System.Drawing.Color.White
        'BorderStyle = System.Windows.Forms.BorderStyle.None
        'ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        'ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        'Dock = System.Windows.Forms.DockStyle.None
        'GridColor = System.Drawing.Color.White
        'Location = New System.Drawing.Point(0, 51)
        'Size = New System.Drawing.Size(300, 300)
        'Me.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single
        'TabIndex = 1
    End Sub
    Dim Row As DataGridViewRow
    ''' <summary>
    ''' Gets The text in the Selected Row Column
    ''' </summary>
    ''' <param name="ColumnName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSelectedRowCellDisplayText(ByVal ColumnName As String) As String
        Try
            If CurrentRow.Index >= 0 Then
                Row = Rows(CurrentRow.Index)
            End If
        Catch ex As Exception
        End Try
        Try

        Catch ex As Exception

        End Try
        Return Row.Cells(ColumnName).Value.ToString
    End Function

    ''' <summary>
    ''' Gets Summary Total of Specified Column
    ''' </summary>
    ''' <param name="ColumnName">The name of the Column you want Total For</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetColumnSumaryText(ByVal ColumnName As String)
        Dim Total As Decimal
        Try
            If CurrentRow.Index >= 0 Then
                Row = Rows(CurrentRow.Index)
                For CurrIndex = 0 To RowCount
                    Row = Rows(CurrIndex)
                    Total += Val(Row.Cells(ColumnName).Value.ToString)
                Next
            End If
        Catch ex As Exception
        End Try
        Return Total
    End Function

    ''' <summary>
    ''' Resize cells evenly across the grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub BestFitAllRowsAndColumns()
        Try
            Dim RowHwidth As Integer
            If RowHeadersVisible = True Then
                RowHwidth = RowHeadersWidth
            End If

            If ColumnCount > 0 Then
                Dim ContainerSize = Me.Width - 60
                Dim ColWidth As Integer = ContainerSize / ColumnCount
                For Col = 0 To ColumnCount - 1
                    Columns(Col).Width = ColWidth
                Next
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub BestFitAllRowsAndColumnsForPrint()
        Try
            Dim RowHwidth As Integer
            If RowHeadersVisible = True Then
                RowHwidth = RowHeadersWidth
            End If

            If ColumnCount > 0 Then
                Dim ContainerSize = OmaxPrintDocument.DefaultPageSettings.PaperSize.Width - 120
                Dim ColWidth As Integer = ContainerSize / ColumnCount
                For Col = 0 To ColumnCount - 1
                    Columns(Col).Width = ColWidth
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub sizechangeee() Handles Me.SizeChanged
        BestFitAllRowsAndColumns()
    End Sub
    Private Sub Opaint() Handles Me.Paint
        'BestFitAllRowsAndColumns()
    End Sub

    Private startPositionS As New Point
    Dim ItemsList As New List(Of String)
    Dim CellValue As String
    ''' <summary>
    ''' Gets the values of selected RowCell as string Collection
    ''' </summary>
    ''' <param name="ColumnName">The name of the Column whos values you want to return</param>
    ''' <remarks></remarks>
    Public Function GetSelectedRowCellValues(ByVal ColumnName As String)
        Try
            ItemsList.Clear()
            For SelectionCounter = 0 To (SelectedCells.Count - 1)
                If SelectedCells(SelectionCounter).OwningColumn.Name = ColumnName Then
                    ItemsList.Add(SelectedCells(SelectionCounter).Value)
                End If
            Next
        Catch ex As Exception

        End Try
        Return ItemsList
    End Function

    Private Sub OmaxGridview1_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles Me.CellPainting
        Try
            If e.RowIndex = -1 AndAlso e.ColumnIndex = Columns.Count - 1 Then
                'e.Paint(e.CellBounds, DataGridViewPaintParts.All And Not DataGridViewPaintParts.ContentForeground)
                'e.Graphics.DrawImage(My.Resources.OmaxGridColumnHeaderImage, e.CellBounds)
                'e.Handled = True

            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Class OmaxImageColumn
        Inherits DataGridViewImageColumn
        Public Sub AddImage(ByVal ColumnImage As Image)
            Me.Image = ColumnImage
            Me.ImageLayout = DataGridViewImageCellLayout.Zoom
        End Sub
        Public Class OmaxColumns
            Inherits DataGridViewColumn
            Public Sub AddColumns(ByVal Columnname As String)
                Me.HeaderText = Columnname
            End Sub
        End Class
    End Class
End Class
