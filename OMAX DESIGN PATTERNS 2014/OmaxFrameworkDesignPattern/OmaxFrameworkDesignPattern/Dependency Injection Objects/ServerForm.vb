﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class ServerForm
    Friend WithEvents ServerForm As New Windows.Forms.Form
    Private AvailableServers As New ArrayList

#Region "Create Server Form"
    Private Sub CreateServerInterface()
        CaptionHeader = New System.Windows.Forms.Label()
        ServerListing = New System.Windows.Forms.ComboBox()
        ConnectionButton = New System.Windows.Forms.Button()
        CancelButton = New System.Windows.Forms.Button()
        WindowsAuth = New System.Windows.Forms.RadioButton()
        ServerAuth = New System.Windows.Forms.RadioButton()
        Panel1 = New System.Windows.Forms.Panel()
        UserNameTextbox = New System.Windows.Forms.TextBox()
        Passwordtextbox = New System.Windows.Forms.TextBox()
        ServerCaption = New System.Windows.Forms.Label()
        UserName = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        Panel1.SuspendLayout()
        ServerForm.SuspendLayout()
        '
        'CaptionHeader
        '
        CaptionHeader.AutoSize = True
        CaptionHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CaptionHeader.Location = New System.Drawing.Point(12, 9)
        CaptionHeader.Name = "CaptionHeader"
        CaptionHeader.Size = New System.Drawing.Size(245, 24)
        CaptionHeader.TabIndex = 0
        CaptionHeader.Text = "Omax Server Connection"
        '
        'ServerListing
        '
        ServerListing.FormattingEnabled = True
        ServerListing.Location = New System.Drawing.Point(13, 65)
        ServerListing.Name = "ServerListing"
        ServerListing.Size = New System.Drawing.Size(361, 21)
        ServerListing.TabIndex = 1
        '
        'ConnectionButton
        '
        ConnectionButton.Location = New System.Drawing.Point(13, 254)
        ConnectionButton.Name = "ConnectionButton"
        ConnectionButton.Size = New System.Drawing.Size(167, 56)
        ConnectionButton.TabIndex = 2
        ConnectionButton.Text = "Connect"
        ConnectionButton.UseVisualStyleBackColor = True
        '
        'CancelButton
        '
        CancelButton.Location = New System.Drawing.Point(208, 254)
        CancelButton.Name = "CancelButton"
        CancelButton.Size = New System.Drawing.Size(166, 56)
        CancelButton.TabIndex = 2
        CancelButton.Text = "Cancel"
        CancelButton.UseVisualStyleBackColor = True
        '
        'WindowsAuth
        '
        WindowsAuth.AutoSize = True
        WindowsAuth.Checked = True
        WindowsAuth.Location = New System.Drawing.Point(13, 104)
        WindowsAuth.Name = "WindowsAuth"
        WindowsAuth.Size = New System.Drawing.Size(140, 17)
        WindowsAuth.TabIndex = 3
        WindowsAuth.TabStop = True
        WindowsAuth.Text = "Windows Authentication"
        WindowsAuth.UseVisualStyleBackColor = True
        '
        'ServerAuth
        '
        ServerAuth.AutoSize = True
        ServerAuth.Location = New System.Drawing.Point(13, 127)
        ServerAuth.Name = "ServerAuth"
        ServerAuth.Size = New System.Drawing.Size(145, 17)
        ServerAuth.TabIndex = 3
        ServerAuth.Text = "Sql Server Authentication"
        ServerAuth.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Panel1.Controls.Add(Passwordtextbox)
        Panel1.Controls.Add(UserNameTextbox)
        Panel1.Controls.Add(PasswordLabel)
        Panel1.Controls.Add(UserName)
        Panel1.Location = New System.Drawing.Point(13, 161)
        Panel1.Name = "Panel1"
        Panel1.Size = New System.Drawing.Size(361, 82)
        Panel1.TabIndex = 4
        '
        'UserNameTextbox
        '
        UserNameTextbox.Location = New System.Drawing.Point(62, 17)
        UserNameTextbox.Name = "UserNameTextbox"
        UserNameTextbox.Size = New System.Drawing.Size(296, 20)
        UserNameTextbox.TabIndex = 0
        '
        'Passwordtextbox
        '
        Passwordtextbox.Location = New System.Drawing.Point(62, 43)
        Passwordtextbox.Name = "Passwordtextbox"
        Passwordtextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Passwordtextbox.Size = New System.Drawing.Size(296, 20)
        Passwordtextbox.TabIndex = 0
        '
        'ServerCaption
        '
        ServerCaption.AutoSize = True
        ServerCaption.Location = New System.Drawing.Point(12, 49)
        ServerCaption.Name = "ServerCaption"
        ServerCaption.Size = New System.Drawing.Size(87, 13)
        ServerCaption.TabIndex = 0
        ServerCaption.Text = "Server Instances"
        '
        'UserName
        '
        UserName.AutoSize = True
        UserName.Location = New System.Drawing.Point(3, 20)
        UserName.Name = "UserName"
        UserName.Size = New System.Drawing.Size(57, 13)
        UserName.TabIndex = 0
        UserName.Text = "UserName"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Location = New System.Drawing.Point(3, 46)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(53, 13)
        PasswordLabel.TabIndex = 0
        PasswordLabel.Text = "Password"
        '
        'OmaxServer
        '
        ServerForm.AcceptButton = ConnectionButton
        ServerForm.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        ServerForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        CancelButton = ConnectionButton
        ServerForm.ClientSize = New System.Drawing.Size(386, 321)
        ServerForm.Controls.Add(Panel1)
        ServerForm.Controls.Add(ServerAuth)
        ServerForm.Controls.Add(WindowsAuth)
        ServerForm.Controls.Add(CancelButton)
        ServerForm.Controls.Add(ConnectionButton)
        ServerForm.Controls.Add(ServerListing)
        ServerForm.Controls.Add(ServerCaption)
        ServerForm.Controls.Add(CaptionHeader)
        ServerForm.MaximizeBox = False
        ServerForm.MaximumSize = New System.Drawing.Size(402, 360)
        ServerForm.MinimizeBox = False
        ServerForm.Name = "OmaxServer"
        ServerForm.ShowIcon = False
        ServerForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        ServerForm.Text = "Server Connection"
        Panel1.ResumeLayout(False)
        Panel1.PerformLayout()
        ServerForm.ResumeLayout(False)
        ServerForm.PerformLayout()

        GetServersAvailable()
        GET_CONNECTION_STRING(ServerListing.Text, UserNameTextbox.Text, Passwordtextbox.Text)
        ServerForm.ShowDialog()

    End Sub
#End Region

#Region "EVENTS HNDLERS"
    Friend WithEvents CaptionHeader As System.Windows.Forms.Label
    Friend WithEvents ServerListing As System.Windows.Forms.ComboBox
    Friend WithEvents ConnectionButton As System.Windows.Forms.Button
    Friend WithEvents CancelButton As System.Windows.Forms.Button
    Friend WithEvents WindowsAuth As System.Windows.Forms.RadioButton
    Friend WithEvents ServerAuth As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Passwordtextbox As System.Windows.Forms.TextBox
    Friend WithEvents UserNameTextbox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordLabel As System.Windows.Forms.Label
    Friend WithEvents UserName As System.Windows.Forms.Label
    Friend WithEvents ServerCaption As System.Windows.Forms.Label

    'Get available server instances
    Private Sub GetServersAvailable()
        Try
            Dim server As String = String.Empty
            Dim Instances As SqlDataSourceEnumerator = SqlDataSourceEnumerator.Instance
            Dim SourceTable As System.Data.DataTable = Instances.GetDataSources()
            For Each row As DataRow In SourceTable.Rows
                Dim Sr_Instance_Names As String = row("InstanceName")
                server = row("ServerName")
                If Sr_Instance_Names <> String.Empty Then
                    server = row("ServerName") & "\" & row("InstanceName")
                End If
                ServerListing.Items.Add(server)
            Next
            ServerListing.SelectedIndex = 0

        Catch ex As Exception

        End Try

    End Sub

    Private Sub GET_CONNECTION_STRING(ByVal SERERNAME As String, ByVal username As String, ByVal password As String)
        Try

            ConnectionString = "Data Source=" & SERERNAME & ";Initial Catalog=master;Integrated Security=True"
            If ServerAuth.Checked = True Then
                If username <> String.Empty And password <> String.Empty Then
                    ConnectionString = "Data Source=" & SERERNAME & ";Initial Catalog=master;" & username & ";" & password & ";"
                Else
                    ConnectionString = "Data Source=" & SERERNAME & ";Initial Catalog=master;Integrated Security=True"
                End If
            Else
                ConnectionString = "Data Source=" & SERERNAME & ";Initial Catalog=master;Integrated Security=True"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private ConnectionString As String
    Dim con, con1 As SqlConnection
    Dim cmd As SqlCommand
    Dim SERVER_OBJECTS As SqlDataReader
    Private Sub ConnectionClick(ByVal Osender As Object, ByVal events As EventArgs) Handles ConnectionButton.Click
        Try
            If ConnectionString <> String.Empty Then

                Dim myquery As String = "SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo." & "enter table name here')"
                Dim nextquerry As String = "select * from INFORMATION_SCHEMA.TABLES ORDER BY TABLE_NAME ASC"
                con = New SqlConnection(ConnectionString)
                con.Open()
                cmd = New SqlCommand(nextquerry, con)
                SERVER_OBJECTS = cmd.ExecuteReader
                While SERVER_OBJECTS.Read
                    TableNameList.Add(New DataBaseTables(SERVER_OBJECTS("TABLE_NAME")))
                End While
                con.Close()
                Dim DT As New SqlDataAdapter(cmd)
                Dim DS As New DataSet
                DT.Fill(DS)
                SERVER_OBJECTS.Close() '

                LoadData()
            End If
        Catch ex As Exception

        End Try


    End Sub
    Public Shared TableNameList As New List(Of DataBaseTables)

    Private Sub AddTable(ByVal table As String)
        TableNameList.Add(New DataBaseTables(table))
    End Sub
    Public Class DataBaseTables
        Private TableNames As String
        Sub New(table As String)
            ' TODO: Complete member initialization 
            TableNames = table
        End Sub

        Public Property Tables As String
            Get
                Return TableNames
            End Get
            Set(ByVal value As String)
                TableNames = value
            End Set
        End Property

    End Class

    Private Sub LoadData()
        Try
            'DataSource = TableNameList
        Catch ex As Exception

        End Try
    End Sub

#End Region
End Class
