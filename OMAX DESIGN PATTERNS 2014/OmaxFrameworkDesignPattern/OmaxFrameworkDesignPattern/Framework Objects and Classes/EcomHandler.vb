﻿Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxData
Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxDateAndTime
Imports OmaxFrameworkDesignPattern.OmaxFramework.Financial

Public Class EcomHandler
    Protected Shared Property BillNumber As Integer
    Public Shared ReciptNumber As Integer = BillNumber
    Private Shared QtyAvailable As Integer

    ''' <summary>
    ''' Omax Payments types
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum PaymentTypes
        Cash
        CreditCard
        DebitCard
        Cheque
    End Enum

    ''' <summary>
    ''' Loads Data From the Inventory 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetProductList()
        Dim List As New BindingSource
        List.DataSource = HandleQuery("Select * From Inventory", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return List
    End Function

    ''' <summary>
    ''' Filters Products
    ''' </summary>
    ''' <param name="Searchtext">The text you want to filter data by</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FilterProducts(ByVal Searchtext As String)
        Dim List As New BindingSource
        List.DataSource = HandleQuery("Select * From Inventory where Description like '%" & Searchtext & "%'", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        If Searchtext = String.Empty Then
            List = GetProductList()
        End If
        Return List
    End Function

    ''' <summary>
    ''' Filters customers list
    ''' </summary>
    ''' <param name="Searchtext"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FilterCustomers(ByVal Searchtext As String)
        Dim List As New BindingSource
        List.DataSource = HandleQuery("Select MiddleName AS [Customer Name],phone,email,DiscountLevel,CustomerID From Customer   where MiddleName like '%" & Searchtext & "%'", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        If Searchtext = String.Empty Then
            List = GetAllCustomer()
        End If
        Return List
    End Function

    ''' <summary>
    ''' Get a List of all customers
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllCustomer()
        Dim List As New BindingSource
        List.DataSource = HandleQuery("Select MiddleName AS [Customer Name],phone,email,DiscountLevel,CustomerID From Customer", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return List
    End Function

    ''' <summary>
    ''' Get the discount group that a customer Belongs to
    ''' </summary>
    ''' <param name="CustomerID">CustomerID for the customer</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCustomerDiscountLevel(ByVal CustomerID As Integer)
        Dim List As New BindingSource
        List.DataSource = HandleQuery("SELECT Customer.MiddleName AS [Customer Name], DISCOUNTS.DiscountPercent FROM  (Customer INNER JOIN   DISCOUNTS ON Customer.DiscountLevel = DISCOUNTS.DiscountLevel) WHERE (Customer.CustomerID = " & CustomerID & ")", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Dim DiscountPercentage As Decimal
        For Each DiscountLevel As DataRowView In List.List
            DiscountPercentage = CType(DiscountLevel.Item("DiscountPercent").ToString, Decimal)
        Next
        Return DiscountPercentage
    End Function

    ''' <summary>
    ''' Return A customer that matches the given CustomerID
    ''' </summary>
    ''' <param name="CustomerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCustomer(ByVal CustomerID As Integer)
        Dim List As New BindingSource
        List.DataSource = HandleQuery("Select * From Customer where CustomerID =" & CustomerID, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return List
    End Function

    ''' <summary>
    ''' Creates a new bill and return the new Bill Number
    ''' </summary>
    ''' <param name="CustomerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function CreateNewBill(ByVal CustomerID As Integer, ByVal ProductId As Integer)
        If CheckProductAvailability(ProductId) = False Then
            MsgBox("Out of Stock")
            BillNumber = 0
        Else
            HandleQuery("Insert INTO ReceiptGenerator (CUSTOMERID,BILLDATE) VALUES (" & CustomerID & ",'" & CurrentDate & "')", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
            Dim Dg As New BindingSource
            Dg.DataSource = HandleQuery("Select Max(billId) From ReceiptGenerator", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
            For Each BillNum As DataRowView In Dg.List
                BillNumber = Val(BillNum.Item(0).ToString)
            Next

        End If
        Return BillNumber
    End Function

    ''' <summary>
    ''' Adds a New Item to Cart
    ''' </summary>
    ''' <param name="CustomerID">Id of the Customer</param>
    ''' <param name="DiscountId">Customers DiscountID</param>
    ''' <param name="ProductId">Id of the Product</param>
    ''' <param name="Qty">Qty of Item to Add</param>
    ''' <param name="Description">Description of the product</param>
    ''' <param name="SellingPrice">SellingPrice of the Product</param>
    ''' <param name="CostPrice">Cost Price of the product</param>
    ''' <param name="lineTax">Total tax</param>
    ''' <param name="LineTotal">Line Toal</param>
    ''' <param name="TaxPercent">TaxPercent of the Item</param>
    ''' <remarks></remarks>
    Public Shared Sub AddItemToCart(ByVal CustomerID As Integer, ByVal DiscountId As Integer, ByVal ProductId As Integer, ByVal Qty As Integer, _
                                         ByVal Description As String, ByVal SellingPrice As Decimal,
                                         CostPrice As Decimal, lineTax As Decimal, LineTotal As Decimal, ByVal TaxPercent As Decimal, ByVal Discount As Decimal)

        'Create a new bill if A new sale is call
        If BillNumber = 0 Then
            CreateNewBill(CustomerID, ProductId)
        End If
        ReciptNumber = BillNumber
        If CheckProductAvailability(ProductId) = False Then
            MsgBox(Description & " Out of Stock")
        ElseIf QtyAvailable < Qty Then
            MsgBox("Quantity on Hand is Less than what is requested. There is only ( " & QtyAvailable & " )" & " in stock.", MsgBoxStyle.Information)
        Else

            HandleQuery("Insert INTO Sales (ProductId,Customerid,DiscountID,SaleDate,ProductDescription,Cost,SellingPrice,TaxPercent,LineTax,LineTotal,Qty,Uom,Billid,Discount) VALUES (" _
                              & ProductId & "," & CustomerID & "," & DiscountId & ",'" & CurrentDate & "','" & Description & "'," & CostPrice & "," & SellingPrice & "," & TaxPercent & "," & lineTax & "," & LineTotal & "," & Qty & "," & "'Each'" & "," & BillNumber & "," & Discount & ")", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)

            UpdateInventoryCount(ProductId, -Qty)
        End If
    End Sub

    ''' <summary>
    ''' Checks Products availability
    ''' </summary>
    ''' <param name="ProductID">The ID of the product to check</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function CheckProductAvailability(ByVal ProductID As Integer) As Boolean
        Dim Dsource As New BindingSource
        Dim Result As Boolean
        Dsource = HandleQuery("Select QtyOnHand From Inventory where ProductID =" & ProductID, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        For Each DataRow As DataRowView In Dsource.List
            If Val(DataRow.Item(0).ToString) = 0 Then
                Result = False
            Else
                Result = True
                QtyAvailable = Val(DataRow.Item(0).ToString)
            End If
        Next
        Return Result
    End Function

    ''' <summary>
    ''' Gets the Updated results of the Customers Cart
    ''' </summary>
    ''' <param name="CustomerId">Customer's ID</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCartItems(ByVal CustomerId As Integer)
        Dim RefreshedCart As BindingSource
        RefreshedCart = HandleQuery("Select SaleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where (CustomerID =" & CustomerId & " AND Billid = " & BillNumber & " )", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return RefreshedCart
    End Function

    ''' <summary>
    ''' Checks Out Customer Cart
    ''' </summary>
    ''' <param name="CustomerID">Id of Customer</param>
    ''' <param name="TotalDue">Bill Total</param>
    ''' <param name="Tax">Total Tax</param>
    ''' <param name="Discount">Discount</param>
    ''' <param name="Tendered">Total Paying</param>
    ''' <param name="Change">Change</param>
    ''' <param name="PaymentType">Payment Type</param>
    ''' <remarks></remarks>
    Public Shared Sub CheckOut(ByVal CustomerID As Integer, ByVal TotalDue As Decimal, ByVal Tax As Decimal, ByVal Discount As Decimal, ByVal Tendered As Decimal, ByVal Change As Decimal, ByVal PaymentType As PaymentTypes)

        HandleQuery("Insert INTO PurchaseSummary (CustomerID,Billid,BillDate,TotalDue,Tax,Discount,Tendered,Change,PaymentType) VALUES (" _
                    & CustomerID & "," & BillNumber & ",'" & CurrentDate & "'," & TotalDue & "," & Tax & "," & Discount & "," & Tendered & "," & Change & ",'" & PaymentType & "')", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
        BillNumber = 0
        MsgBox("Check Out successfully!")

    End Sub

    ''' <summary>
    ''' Removes Item from Cart
    ''' </summary>
    ''' <param name="SaleId">Id of Item in cart</param>
    ''' <param name="CustomerID">Customer ID</param>
    ''' <param name="Qty">Qty removing</param>
    ''' <param name="ProductID"> Product Id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RemoveItem(ByVal SaleId As Integer, ByVal CustomerID As Integer, ByVal Qty As Integer, ByVal ProductID As Integer)
        Dim RefreshedCart As BindingSource
        HandleQuery("Delete From Sales where Saleid =" & SaleId, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
        RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where (CustomerID =" & CustomerID & " AND Billid = " & BillNumber & " )", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        UpdateInventoryCount(ProductID, -Qty)
        Return RefreshedCart
    End Function

    ''' <summary>
    ''' Updates the Inventory Products Qty
    ''' </summary>
    ''' <param name="ProductID"></param>
    ''' <param name="Qty_"></param>
    ''' <remarks></remarks>
    Private Shared Sub UpdateInventoryCount(ByVal ProductID As Integer, ByVal Qty_ As Integer)
        HandleQuery("Update Inventory Set QtyOnHand = QtyOnHand+" & Qty_ & " Where ProductID =" & ProductID, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
    End Sub

    Private Shared Function ChangeQty(ByVal SaleId As Integer, ByVal CustomerID As Integer, ByVal Qty As Integer, ByVal ProductID As Integer)
        Dim RefreshedCart As BindingSource
        HandleQuery("Delete From Sales where Saleid =" & SaleId, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
        RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where (CustomerID =" & CustomerID & " AND Billid = " & BillNumber & " )", My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return RefreshedCart
    End Function

    ''' <summary>
    ''' Cancels the current sale
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CancelSale()
        Dim RefreshedCart As BindingSource
        RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where Billid = " & BillNumber, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        For Each Product As DataRowView In RefreshedCart.List
            UpdateInventoryCount(Val(Product.Item("ProductID")), -Val(Product.Item("Qty")))
        Next
        HandleQuery("Delete From Sales where BillID =" & BillNumber, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
        RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where Billid = " & BillNumber, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Return RefreshedCart
    End Function

    ''' <summary>
    ''' Adds Selected Product in OmaxGridview to customers cart
    ''' </summary>
    ''' <param name="CustomerIds">Customer Id</param>
    ''' <param name="Qty">Qty to Add</param>
    ''' <param name="ProductIDList">The slected Products in a List</param>
    ''' <remarks></remarks>
    Public Shared Sub AddSelectedProducts(ByVal CustomerIds As Integer, ByVal Qty As Integer, ByVal ProductIDList As List(Of String))
        Try
            For Each ProductIDs As Integer In ProductIDList
                Dim RefreshedCart As BindingSource
                RefreshedCart = HandleQuery("Select * From Inventory Where ProductID = " & ProductIDs, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
                For Each DataRow As DataRowView In RefreshedCart.List
                    AddItemToCart(CustomerIds, 1, ProductIDs _
                                                               , Qty _
                                                               , DataRow.Item("Description").ToString _
                                                               , Val(DataRow.Item("SellingPrice").ToString) _
                                                               , Val(DataRow.Item("CostPrice").ToString) _
                                                               , Val(GetPercentageValue(((Val(DataRow.Item("SellingPrice").ToString) - Val(GetPercentageValue(Val(DataRow.Item("SellingPrice").ToString), Val(GetCustomerDiscountLevel(CustomerIds))))) * Qty) _
                                                                                        , Val(DataRow.Item("TaxPercent").ToString))) _
                                                               , Val(AddPercentage((Val(DataRow.Item("SellingPrice").ToString) - Val(GetPercentageValue(Val(DataRow.Item("SellingPrice").ToString), GetCustomerDiscountLevel(CustomerIds)))) _
                                                                                   * Qty, Val(DataRow.Item("TaxPercent").ToString))) _
                                                               , Val(DataRow.Item("TaxPercent").ToString) _
                                                               , Val(GetPercentageValue(Val(DataRow.Item("SellingPrice").ToString), GetCustomerDiscountLevel(CustomerIds))) * Qty)
                Next
            Next
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Removes selected items from customer's cart
    ''' </summary>
    ''' <param name="ProductIDList">List of items to remove</param>
    ''' <remarks></remarks>
    Public Shared Function RemoveSelectedProducts(ByVal ProductIDList As List(Of String))
        Dim RefreshedCart As New BindingSource
        For Each SaleIDs As Integer In ProductIDList

            RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where SaleID = " & SaleIDs, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
            For Each Product As DataRowView In RefreshedCart.List
                UpdateInventoryCount(Val(Product.Item("ProductID")), -Val(Product.Item("Qty")))
            Next
            HandleQuery("Delete From Sales where saleID =" & SaleIDs, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.NoneQuery)
            RefreshedCart = HandleQuery("Select saleID,ProductId,ProductDescription as Description,SellingPrice as Price,LineTax,LineTotal,Qty,Uom,Discount From Sales where Billid = " & BillNumber, My.MySettings.Default.OmaxShoppingConnectionString, DataBaseType.Access, QueryType.ScalarQuery)
        Next
        Return RefreshedCart
    End Function

End Class
