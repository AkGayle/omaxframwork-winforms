﻿Imports System.ComponentModel
Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Net.Mail
Imports System.IO.Ports
Imports System.IO
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop


Public Class OmaxFramework

#Region "Enumerations"

#End Region

#Region "Declrations"

#End Region

    ''' <summary>
    ''' Date and Time class to properly manage date and time by extracting certain information
    ''' </summary>
    ''' <remarks>Use to manipulate date and time</remarks>
    Public Class OmaxDateAndTime

        ''' <summary>
        ''' Gets Current date from System Date
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared CurrentDate As DateTime = DateTime.Now.ToString

        ''' <summary>
        ''' Exposes the total working days in a month in work weeks that is from Monday to Friday
        ''' </summary>
        ''' <param name="Month">Month</param>
        ''' <param name="Year">Year</param>
        ''' <returns>Gets the total working days in month as integer </returns>
        ''' <remarks></remarks>
        Public Shared Function DaysInMonth(Optional ByVal Month As Months = Months.CurrentMonth, Optional ByVal Year As Integer = 0, Optional ByVal Day As Days = Days.Defaults)
            If Year = 0 Then
                Year = DateTime.Now.Year
            End If
            If Month = Months.CurrentMonth Then
                Month = GetMonth()
            End If
            Dim WorkingDays As Integer
            Dim StartDate As DateTime = CType(Month & "/" & GetMonthStart() & "/" & Year, DateTime)
            Dim EndDate As DateTime = CType(Month & "/" & GetMonthEnd(Month) & "/" & Year, DateTime)
            While StartDate < EndDate
                If Day = Days.WorkingDays Then
                    If StartDate.DayOfWeek <> DayOfWeek.Saturday And StartDate.DayOfWeek <> DayOfWeek.Sunday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Sunday Then
                    If StartDate.DayOfWeek = DayOfWeek.Sunday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Monday Then
                    If StartDate.DayOfWeek = DayOfWeek.Monday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Tuesday Then
                    If StartDate.DayOfWeek = DayOfWeek.Tuesday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Wednesday Then
                    If StartDate.DayOfWeek = DayOfWeek.Wednesday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Thursday Then
                    If StartDate.DayOfWeek = DayOfWeek.Thursday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Friday Then
                    If StartDate.DayOfWeek = DayOfWeek.Friday Then
                        WorkingDays += 1
                    End If
                ElseIf Day = Days.Saturday Then
                    If StartDate.DayOfWeek = DayOfWeek.Sunday Then
                        WorkingDays += 1
                    End If
                End If

                If Day = Days.Defaults Then
                    WorkingDays += 1
                End If

                StartDate = StartDate.AddDays(1)
            End While
            If Day = Days.Defaults Then
                WorkingDays += 1
            End If
            Return WorkingDays
        End Function

        ''' <summary>
        ''' Gets Current month as integer
        ''' </summary>
        ''' <param name="Month">Month</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetMonth(Optional ByVal Month As Months = Months.CurrentMonth)
            Dim Cmonth As Integer

            Cmonth = DateTime.Now.Month
            Select Case Cmonth
                Case 1
                    Month = Months.January
                Case 2
                    Month = Months.Fabruary
                Case 3
                    Month = Months.March
                Case 4
                    Month = Months.April
                Case 5
                    Month = Months.May
                Case 6
                    Month = Months.June
                Case 7
                    Month = Months.July
                Case 8
                    Month = Months.August
                Case 9
                    Month = Months.September
                Case 10
                    Month = Months.October
                Case 11
                    Month = Months.November
                Case 12
                    Month = Months.December
            End Select
            Return Month
        End Function

        ''' <summary>
        ''' Month enumerator
        ''' </summary>
        ''' <remarks></remarks>
        Public Enum Months
            ''' <summary>
            ''' Gets the current Month from system Date
            ''' </summary>
            ''' <remarks></remarks>
            CurrentMonth
            January
            Fabruary
            March
            April
            May
            June
            July
            August
            September
            October
            November
            December
        End Enum

        ''' <summary>
        ''' Get The start of a Month as string
        ''' </summary>
        ''' <param name="Month">Month and year. Format MMMM,yyyy</param>
        ''' <returns>Gets the start of a Month</returns>
        ''' <remarks></remarks>
        ''' 

        Public Shared Function GetMonthStart(Optional ByVal Month As Months = Months.CurrentMonth, Optional ByVal Year As Integer = 0)
            Dim MonthStart As String
            MonthStart = "01"
            Return MonthStart
        End Function

        ''' <summary>
        ''' Gets the End of a Month as integer
        ''' </summary>
        ''' <param name="Month">Month and year format MMMM,yyyy</param>
        ''' <returns>Gets the End of month as Integer</returns>
        ''' <remarks></remarks>
        Public Shared Function GetMonthEnd(Optional ByVal Month As Integer = 0, Optional ByVal Year As Integer = 0)
            If Month = 0 Then
                Month = DateTime.Now.Month
            End If
            If Year = 0 Then
                Year = DateTime.Now.Year
            End If

            Dim MonthStart As Integer
            MonthStart = DateTime.DaysInMonth(Year, Month)
            Return MonthStart
        End Function

        ''' <summary>
        ''' Eposes Days option
        ''' </summary>
        ''' <remarks></remarks>
        Public Enum Days
            Sunday
            Monday
            Tuesday
            Wednesday
            Thursday
            Friday
            Saturday
            WorkingDays
            Defaults
        End Enum

        Public Shared Function AddMonth(ByVal StartMonth As Months, ByVal StartYear As Integer, ByVal MonthsToAdd As Integer)
            Dim Months_ As String
            Dim TempMonth As Integer = StartMonth
            If StartMonth = Months.CurrentMonth Then
                TempMonth = DateTime.Now.Month
            End If

            Months_ = CType(TempMonth.ToString, DateTime).ToString("MMMM") & "," & StartYear
            Months_ = CType(Months_, DateTime).AddMonths(1)
            Return Months_
        End Function

        Public Shared Function DaysCount(ByVal QueryDate As DateTime)
            Dim ResultSet As String = DateDiff(DateInterval.Day, QueryDate, QueryDate)
            Return ResultSet
        End Function


    End Class


    Public Class Financial

        Public Class AutoLoanStructure

#Region "Control Structures"

            Private Payment As Decimal
            Private Interest As Decimal
            Private Principal As Decimal
            Private Balance As Decimal
            Private Months As Integer

            Public Sub New(ByVal Payments As Decimal, ByVal Interests As Decimal, ByVal Principals As Decimal, ByVal Balances As Decimal, ByVal Month_ As Integer)
                Payment = Payments
                Interest = Interests
                Principal = Principals
                Balance = Balances
                Months = Month_
            End Sub

            'Get Payments
            Public Property Payments() As Decimal
                Get
                    Return Payment
                End Get
                Set(ByVal value As Decimal)
                    Payment = value
                End Set
            End Property

            'Get Interests
            Public Property Interests() As Decimal
                Get
                    Return Interest
                End Get
                Set(ByVal value As Decimal)
                    Interest = value
                End Set
            End Property

            'Get Principal
            Public Property Principals() As Decimal
                Get
                    Return Principal
                End Get
                Set(ByVal value As Decimal)
                    Principal = value
                End Set
            End Property

            'Get Balance
            Public Property Balances() As Decimal
                Get
                    Return Balance
                End Get
                Set(ByVal value As Decimal)
                    Balance = value
                End Set
            End Property

            Public Property Month As Integer
                Get
                    Return Months
                End Get
                Set(ByVal value As Integer)
                    Months = value
                End Set
            End Property

#End Region

        End Class

        Public Property TotalPayment As Decimal
        Public Property TotalInterest As Decimal
        Public Shared AutoLoanResultSet As New BindingList(Of AutoLoanStructure)
        Public Interest As Decimal
        ''' <summary>
        ''' Calculates Auto Loan Monthly Payments 
        ''' </summary>
        ''' <param name="InterestRate">Interest Rate on Loan</param>
        ''' <param name="LoanPeriod">The period that the loan will run for eg Period = 12 for (1 Year monthly Payments) or 24 for (2 Years monthly Payment)s </param>
        ''' <param name="LoanAmount">The Lumpsome amount to be paid</param>
        ''' <param name="DownPayment">The sum of all payments already paid on loan</param>
        ''' <param name="PaymentInterval">The time of Month payments are Due Begginning of the Month or End of the Month</param>
        ''' <returns>Returns a list of future payments</returns>
        ''' <remarks></remarks>
        Public Function AutoLoanPaymentToList(ByVal InterestRate As Decimal, ByVal LoanPeriod As Decimal, ByVal LoanAmount As Decimal, Optional ByVal DownPayment As Decimal = 0, Optional ByVal PaymentInterval As DueDate = DueDate.EndOfPeriod) As Double
            Dim Interest As Decimal
            Dim Payment As Decimal
            Dim PeriodCount As Integer
            Dim Balance As Decimal
            Dim Principal As Decimal
            Dim Month As Integer

            LoanAmount -= DownPayment
            Dim TempConstPayment As Decimal = Math.Round(Pmt((InterestRate / 100) / 12, LoanPeriod, -LoanAmount, 0, DueDate.EndOfPeriod), 2)
            Balance = Math.Round(LoanAmount, 2)

            While PeriodCount <> LoanPeriod
                Month += 1
                Interest = Math.Round(((InterestRate / 100) / 12) * LoanAmount, 2)
                Principal = Math.Round((TempConstPayment - Interest), 2)
                AutoLoanResultSet.Add(New AutoLoanStructure(TempConstPayment, Interest, Principal, Balance, Month))
                TotalInterest += Interest
                LoanAmount += Interest
                LoanAmount -= TempConstPayment
                Balance = Math.Round(LoanAmount, 2)
                PeriodCount += 1
                TotalPayment += TempConstPayment
            End While
            Month += 1
            AutoLoanResultSet.Add(New AutoLoanStructure(0, 0, 0, 0, Month))

            Return Payment
        End Function

        ''' <summary>
        ''' Calculates Auto Loan Payments 
        ''' </summary>
        ''' <param name="InterestRate">Interest Rate on Loan</param>
        ''' <param name="LoanPeriod">The period that the loan will run for eg Period = 12 for (1 Year monthly Payments) or 24 for (2 Years monthly Payment)s </param>
        ''' <param name="LoanAmount">The Lumpsome amount to be paid</param>
        ''' <param name="DownPayment">The sum of all payments already paid on loan</param>
        ''' <param name="PaymentInterval">The time of Month payments are Due Begginning of the Month or End of the Month</param>
        ''' <returns>Returns a list of future payments</returns>
        ''' <remarks></remarks>
        Public Shared Function AutoLoanPayment(ByVal InterestRate As Decimal, ByVal LoanPeriod As Decimal, ByVal LoanAmount As Decimal, Optional ByVal DownPayment As Decimal = 0, Optional ByVal PaymentInterval As DueDate = DueDate.EndOfPeriod) As Double
            Dim Payment As Decimal
            Dim Balance As Decimal
            Dim Principal As Decimal
            Dim Interest As Decimal
            LoanAmount -= DownPayment

            Payment = Pmt((InterestRate / 100) / 12, LoanPeriod, -LoanAmount, 0, DueDate.EndOfPeriod)
            Interest = ((InterestRate / 100) / 12) * LoanAmount
            Balance = (LoanAmount - Payment) + Interest
            Principal = LoanAmount - Balance
            LoanAmount -= Payment

            Return Payment
        End Function

        Public Shared Function GetPercentage(ByVal TotalValue As Decimal, ByVal Percentvalue As Decimal)
            If TotalValue <= 0 Or Percentvalue <= 0 Then
                Return 0
            Else
                Return (Percentvalue / TotalValue) * 100
            End If
        End Function

        Public Shared Function AddPercentage(ByVal TotalValue As Decimal, ByVal Percentvalue As Decimal)
            If TotalValue <= 0 Or Percentvalue <= 0 Then
                Return 0
            Else
                Return ((Percentvalue / 100) * TotalValue) + TotalValue
            End If
        End Function

        ''' <summary>
        ''' Gets the Discount amount in cash from a Percentage value
        ''' </summary>
        ''' <param name="TotalValue">Total Price</param>
        ''' <param name="Percentvalue">Percentage</param>
        ''' <returns>The The value of the percentage amount</returns>
        ''' <remarks></remarks>
        Public Shared Function GetPercentageValue(ByVal TotalValue As Decimal, ByVal Percentvalue As Decimal)
            If TotalValue <= 0 Or Percentvalue <= 0 Then
                Return 0
            Else
                DiscountAmount = (Percentvalue / 100) * TotalValue
                Return (Percentvalue / 100) * TotalValue
            End If
        End Function

        Private Shared Property DiscountPrice As Decimal
        Private Shared Property DiscountAmount As Decimal
        ''' <summary>
        ''' Applies Discount to a total
        ''' </summary>
        ''' <param name="TotalValue">The toal Price or cost</param>
        ''' <param name="DiscountPercentage">The percentage Discount you want to apply</param>
        ''' <param name="TaxPercentage">The tax percentage on the total</param>
        ''' <returns>Returns the Discounted price</returns>
        ''' <remarks></remarks>
        Public Shared Function ApplyDiscount(ByVal TotalValue As Decimal, ByVal DiscountPercentage As Decimal, Optional ByVal TaxPercentage As Decimal = 0)
            DiscountPrice = TotalValue - GetPercentageValue(TotalValue, DiscountPercentage)
            If TaxPercentage > 0 Then
                DiscountPrice = AddPercentage(TotalValue - GetPercentageValue(TotalValue, DiscountPercentage), TaxPercentage)
            End If
            Return DiscountPrice
        End Function

    End Class

    Public Class OmaxImage
        Public Shared Property CurrentItem As Integer
        Public Class OmaxImageList

            Private ImageCollection As Byte
            Public Property Images As Byte
                Get
                    Return ImageCollection
                End Get
                Set(ByVal value As Byte)
                    ImageCollection = value
                End Set
            End Property


            Public Sub New(Optional ByVal Image_ As Byte = Nothing)
                If IsNothing(Image_) = False Then
                    Images = Image_
                End If
            End Sub

        End Class

        Public Shared ImageCollection As New List(Of OmaxImageList)
        Public Shared Sub AddImage(ByVal Image As Byte)
            ImageCollection.Add(New OmaxImageList(Image))
            CurrentItem += 1
        End Sub



        ''' <summary>
        ''' A MemoryStream to hold images from the database as byte to convert to image
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared NewImage As IO.MemoryStream
        ''' <summary>
        ''' Converts bytes from data fields in database with DataTypes Varbinary or Image to System.Drawing.Image
        ''' </summary>
        ''' <param name="PicMemoryStream">The data from the Image field in your database as Byte() or Byte[]</param>
        ''' <returns>Gets a system.Drawing>Image representatin of the converted Byte()</returns>
        ''' <remarks>Use to rneder database images to display in Picturebox</remarks>
        Public Shared Function GetPicture(ByVal PicMemoryStream As Byte())
            If PicMemoryStream Is Nothing Then
                ' Do nothing
                Return Nothing
            Else
                Dim PictureStream As New IO.MemoryStream(PicMemoryStream)
                NewImage = PictureStream
                Return PictureStream
            End If
        End Function


    End Class


    Public Class OmaxData

        Public Shared Function GetServerDateAndTime(ByVal ServerConnectionString As String)
            Dim ReturnedDate As DateTime
            Try

                Dim CON As String = ServerConnectionString
                Dim CONNECTIONS As New SqlClient.SqlConnection
                CONNECTIONS.ConnectionString = CON
                CONNECTIONS.Open()
                Dim DS As New DataSet
                Dim TD As New SqlClient.SqlDataAdapter

                TD = New SqlClient.SqlDataAdapter("use master SELECT GETDATE() AS TODAYDATE", CONNECTIONS)
                TD.Fill(DS)
                Dim Ilist As New BindingSource
                Ilist.DataSource = DS.Tables(0)

                CONNECTIONS.Close()

                For Each IDS As DataRowView In Ilist
                    ReturnedDate = CType(IDS.Item(0).ToString, DateTime)

                Next
            Catch ex As Exception
                MsgBox(ex.Message & " --> StackTrace " & ex.StackTrace)
            End Try
            Return ReturnedDate
        End Function

        ''' <summary>
        ''' Query Types for executing Update,Insert, Delete, select or other transactional query
        ''' NonQuery For Update, Insert and Delete
        ''' Scalar For select query that will return rows
        ''' </summary>
        ''' <remarks></remarks>
        ''' 

        Public Enum QueryType
            ''' <summary>
            ''' Perform Update, Insert and Delete commands
            ''' </summary>
            ''' <remarks></remarks>
            NoneQuery
            ''' <summary>
            ''' Perform Query that returns rows
            ''' </summary>
            ''' <remarks></remarks>
            ScalarQuery
        End Enum
        Public Enum DataBaseType
            Access
            Excel
            Sql
        End Enum

        ''' <summary>
        ''' For returning Datarows on query
        ''' </summary>
        ''' <remarks></remarks>
        Private ScalarDatasource As New BindingSource

        Public Shared Property RowCount As Integer
        Public Shared Function HandleQuery(ByVal QueryText As String, ByVal ConnectionString As String, ByVal DatabaseTypes As DataBaseType, Optional ByVal QueryType As QueryType = OmaxData.QueryType.ScalarQuery)
            Dim isource As New BindingSource
            Try

                If DatabaseTypes = DataBaseType.Access Or DatabaseTypes = DataBaseType.Excel Then
                    Dim MyCommandAccess As OleDb.OleDbDataAdapter
                    Dim MyConnectionAccess As OleDb.OleDbConnection
                    MyConnectionAccess = New OleDb.OleDbConnection(ConnectionString)
                    MyConnectionAccess.Open()

                    'NoneQuery
                    If QueryType = OmaxData.QueryType.NoneQuery Then
                        If DatabaseTypes = DataBaseType.Access Or DatabaseTypes = DataBaseType.Excel Then
                            Dim Command As New OleDbCommand
                            Command.CommandType = CommandType.Text
                            Command.Connection = MyConnectionAccess
                            Command.CommandText = QueryText
                            Command.ExecuteNonQuery()
                        End If
                    End If


                    'ScalarQuery
                    If QueryType = OmaxData.QueryType.ScalarQuery Then

                        Dim DS As New DataSet
                        If DatabaseTypes = DataBaseType.Access Or DatabaseTypes = DataBaseType.Excel Then
                            MyCommandAccess = New OleDb.OleDbDataAdapter(QueryText, MyConnectionAccess)
                            RowCount = MyCommandAccess.Fill(DS)
                            isource.DataSource = DS.Tables(0)
                        End If
                    End If
                    MyConnectionAccess.Close()

                End If

                If DatabaseTypes = DataBaseType.Sql Then
                    Dim CON As String = ConnectionString
                    Dim CONNECTIONS As New SqlClient.SqlConnection
                    CONNECTIONS.ConnectionString = CON
                    CONNECTIONS.Open()

                    'NoneQuery
                    If QueryType = OmaxData.QueryType.NoneQuery Then
                        If DatabaseTypes = DataBaseType.Sql Then
                            Dim Command As New SqlCommand
                            Command.CommandType = CommandType.Text
                            Command.Connection = CONNECTIONS
                            Command.CommandText = QueryText
                            Command.ExecuteNonQuery()
                        End If

                    End If

                    'ScalarQuery
                    If QueryType = OmaxData.QueryType.ScalarQuery Then

                        Dim DS As New DataSet
                        If DatabaseTypes = DataBaseType.Sql Then
                            Dim TD As New SqlClient.SqlDataAdapter
                            TD = New SqlClient.SqlDataAdapter(QueryText, CONNECTIONS)
                            RowCount = TD.Fill(DS)
                            isource.DataSource = DS.Tables(0)
                        End If

                    End If
                    CONNECTIONS.Close()

                End If

            Catch ex As Exception
                MsgBox(ex.Message & " --> StackTrace " & ex.StackTrace)
            End Try

            Return isource

        End Function

        Public Shared Function ImportExcelData(ByVal FilePath As String, Optional ByVal SheetName As String = "")

            Dim ROWCOUNTS As Integer
            Dim ProcessString As String

            Dim SheetList As New ArrayList
            Dim objExcel As Excel.Application
            Dim objWorkBook As Excel.Workbook
            Dim objWorkSheets As Excel.Worksheet
            Dim ExcelSheetName As String = ""
            Dim DS As DataSet

            'connecting to excel file
            ProcessString = "Connecting to datasource"

            objExcel = CreateObject("Excel.Application")
            objWorkBook = objExcel.Workbooks.Open(FilePath)

            If SheetName <> String.Empty Then
                SheetList.Add(SheetName)
            End If

            For Each objWorkSheets In objWorkBook.Worksheets
                SheetList.Add(objWorkSheets.Name)
            Next
            ProcessString = "Gathering Data"

            Dim MyCommand As OleDb.OleDbDataAdapter
            Dim MyConnection As OleDb.OleDbConnection

            MyConnection = New OleDb.OleDbConnection( _
                 "provider=Microsoft.Jet.OLEDB.4.0; " & _
                 "data source=" & FilePath & "; " & _
                 "Extended Properties=Excel 8.0;")

            ' Select the data from Sheet1 of the workbook.
            MyCommand = New OleDb.OleDbDataAdapter( _
                 "select * from [" & SheetList(0) & "$]", MyConnection)
            DS = New System.Data.DataSet()
            MyCommand.Fill(DS)
            Dim Bindingsource_PX As New BindingSource
            Bindingsource_PX.DataSource = DS.Tables(0).DefaultView

            'Get total FIles to Import
            For Each Rowcount As DataRowView In Bindingsource_PX.List
                ROWCOUNTS += 1
                ProcessString = "Processing data No. " & ROWCOUNTS & " (" & Rowcount.Item(0).ToString & " )"
            Next

            ProcessString = "Imports Completed"
            objExcel.Quit()

            Return Bindingsource_PX
        End Function

        ''' <summary>
        ''' Returns a list of Available servers on a machine
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetServersAvailable()
            Dim Lists As New List(Of String)
            Try

                Dim server As String = String.Empty
                Dim Instances As SqlDataSourceEnumerator = SqlDataSourceEnumerator.Instance
                Dim SourceTable As System.Data.DataTable = Instances.GetDataSources()
                For Each row As DataRow In SourceTable.Rows
                    Dim Sr_Instance_Names As String = row("InstanceName")
                    server = row("ServerName")
                    If Sr_Instance_Names <> String.Empty Then
                        server = row("ServerName") & "\" & row("InstanceName")
                    End If
                    Lists.Add(server)
                Next
            Catch ex As Exception

            End Try
            Return lists
        End Function

    End Class


    Public Class InformationHiding
        Private Shared PassWords As String
        Public Shared ReturnDecrypted As String
        Private Shared FakeReturn As String
        Public Shared ReturnIllusion As String

        ''' <summary>
        ''' Makes an illusion by swapping out the original text with random letters and numbers
        ''' </summary>
        ''' <param name="PassWord">The text you want to make an illusion out of</param>
        ''' <returns>Gets the coded text as an illusion</returns>
        ''' <remarks>This is a beta version that does limited code generation.Full version coming soon</remarks>

        Public Shared Function MakeIllusion(ByVal PassWord As String)
            PassWords = ""
            ReturnIllusion = ""
            For Each CHARC As Char In PassWord
                KeyMaker(CHARC)
            Next
            Return ReturnIllusion
        End Function

        Private Shared Sub EncryptParser(ByVal Characters As String)
            PassWords += Characters
            FakeReturn += Characters
            ReturnIllusion += Characters
        End Sub

        Private Shared Sub Unrav(ByVal characters As String)
            ReturnDecrypted += characters
        End Sub

        Private Shared sets As String

        ''' <summary>
        ''' This will return the decoded information from the illusion set of texts
        ''' </summary>
        ''' <param name="Character">This is the illusion text that is generated by Function MakeIllusion</param>
        ''' <returns>Gets the decoded information from the illusion set of texts</returns>
        ''' <remarks>This is a beta version that does limited code generation.Full version coming soon</remarks>
        ''' 

        Public Shared Function GetDecoded(ByVal Character As String)
            ReturnDecrypted = ""
            Try
                While Character.Length <> 0
                    InverseKeyMaker(Character.Substring(0, 5))
                    sets = Character.Substring(0, 5)
                    Character = Character.Remove(0, 5)
                End While
            Catch ex As Exception

            End Try

            Return ReturnDecrypted
        End Function

#Region "ENCRYPTION AND DECRYPT"

        Private Shared Sub KeyMaker(ByVal Characters As String)
            Select Case Characters.ToUpper
                Case "A"
                    EncryptParser("iou22")
                Case "B"
                    EncryptParser("wwedd")
                Case "C"
                    EncryptParser("hrtrw")
                Case "D"
                    EncryptParser("yjjwe")
                Case "E"
                    EncryptParser("oujhh")
                Case "F"
                    EncryptParser("errth")
                Case "G"
                    EncryptParser("ethgf")
                Case "H"
                    EncryptParser("ioyre")
                Case "I"
                    EncryptParser("pkjbs")
                Case "J"
                    EncryptParser("wbfcq")
                Case "K"
                    EncryptParser("iokje")
                Case "L"
                    EncryptParser("mbnvv")
                Case "M"
                    EncryptParser("qwaxd")
                Case "N"
                    EncryptParser("pkefh")
                Case "O"
                    EncryptParser("noprf")
                Case "P"
                    EncryptParser("qaxcd")
                Case "Q"
                    EncryptParser("vbghn")
                Case "R"
                    EncryptParser("qswcv")
                Case "S"
                    EncryptParser("thgds")
                Case "T"
                    EncryptParser("hjght")
                Case "U"
                    EncryptParser("wffvb")
                Case "V"
                    EncryptParser("mplgr")
                Case "W"
                    EncryptParser("hpfdj")
                Case "X"
                    EncryptParser("wdvgf")
                Case "Y"
                    EncryptParser("yukjk")
                Case "Z"
                    EncryptParser("ewfvb")
                Case " "
                    EncryptParser("X6_PP")
                Case "_"
                    EncryptParser("UVCKK")
                Case "."
                    EncryptParser("TICLL")
                Case "0"
                    EncryptParser("8dfd5")
                Case "1"
                    EncryptParser("f71sf")
                Case "2"
                    EncryptParser("g0g2k")
                Case "3"
                    EncryptParser("0df3g")
                Case "4"
                    EncryptParser("8s1a2")
                Case "5"
                    EncryptParser("q3d8d")
                Case "6"
                    EncryptParser("i4jk7")
                Case "7"
                    EncryptParser("c2vdx")
                Case "8"
                    EncryptParser("3wsdp")
                Case "9"
                    EncryptParser("4f5sv")

            End Select
        End Sub

        Private Shared Sub InverseKeyMaker(ByVal Characters As String)
            Select Case Characters
                Case "iou22"
                    Unrav("A")
                Case "wwedd"
                    Unrav("B")
                Case "hrtrw"
                    Unrav("C")
                Case "yjjwe"
                    Unrav("D")
                Case "oujhh"
                    Unrav("E")
                Case "errth"
                    Unrav("F")
                Case "ethgf"
                    Unrav("G")
                Case "ioyre"
                    Unrav("H")
                Case "pkjbs"
                    Unrav("I")
                Case "wbfcq"
                    Unrav("J")
                Case "iokje"
                    Unrav("K")
                Case "mbnvv"
                    Unrav("L")
                Case "qwaxd"
                    Unrav("M")
                Case "pkefh"
                    Unrav("N")
                Case "noprf"
                    Unrav("O")
                Case "qaxcd"
                    Unrav("P")
                Case "vbghn"
                    Unrav("Q")
                Case "qswcv"
                    Unrav("R")
                Case "thgds"
                    Unrav("S")
                Case "hjght"
                    Unrav("T")
                Case "wffvb"
                    Unrav("U")
                Case "mplgr"
                    Unrav("V")
                Case "hpfdj"
                    Unrav("W")
                Case "wdvgf"
                    Unrav("X")
                Case "yukjk"
                    Unrav("Y")
                Case "ewfvb"
                    Unrav("Z")
                Case "X6_PP"
                    Unrav(" ")
                Case "UVCKK"
                    Unrav("_")
                Case "TICLL"
                    Unrav(".")
                Case "8dfd5"
                    Unrav("0")
                Case "f71sf"
                    Unrav("1")
                Case "g0g2k"
                    Unrav("2")
                Case "0df3g"
                    Unrav("3")
                Case "8s1a2"
                    Unrav("4")
                Case "q3d8d"
                    Unrav("5")
                Case "i4jk7"
                    Unrav("6")
                Case "c2vdx"
                    Unrav("7")
                Case "3wsdp"
                    Unrav("8")
                Case "4f5sv"
                    Unrav("9")

            End Select
        End Sub

#End Region

    End Class

    Public Class OmaxMail
        ''' <summary>
        ''' USE TO SEND EMAIL FROM GMAIL IMAP
        ''' </summary>
        ''' <param name="ToAddress">The address of the person or Persons you are sending the mail to. If address is more than ! then Address must be seperated by a comma</param>
        ''' <param name="Attachments">Attachments sending with messae. leave blank if there is no attachment</param>
        ''' <param name="Subject">Subject Of the message</param>
        ''' <param name="Message">The body of the message</param>
        ''' <param name="GmailAddress">Your Gmail Account Name</param>
        ''' <param name="GmailPassword">Your Gmail Account Password</param>
        ''' <returns>Returns message status Sent if message is sent successfully or an error message indicating errors encountered</returns>
        ''' <remarks></remarks>
        ''' 
        Public Shared Function SendEmail(ByVal ToAddress As String, ByVal Attachments As String, ByVal Subject As String, ByVal Message As String, ByVal GmailAddress As String, ByVal GmailPassword As String)
            Dim MessageConfirmation As String
            Try
                Dim EmailMessage As New MailMessage
                EmailMessage.From = New MailAddress(GmailAddress)
                EmailMessage.To.Add(ToAddress)
                EmailMessage.Subject = Subject

                If Attachments <> String.Empty And IO.File.Exists(Attachments) = True Then
                    Dim MYATTCHMENT As New Attachment(Attachments)
                    EmailMessage.Attachments.Add(MYATTCHMENT)
                End If

                EmailMessage.Body = Message
                EmailMessage.IsBodyHtml = False
                Dim smtp As New SmtpClient("smtp.gmail.com")
                smtp.Port = 587
                smtp.EnableSsl = True

                smtp.Credentials = New System.Net.NetworkCredential(GmailAddress, GmailPassword)

                System.Threading.Thread.Sleep(100)

                smtp.Timeout = 600000
                smtp.Send(EmailMessage)

                MessageConfirmation = "Message Sent to " & ToAddress
            Catch ex As Exception
                MessageConfirmation = ex.Message
            End Try

            Return MessageConfirmation
        End Function


    End Class


End Class