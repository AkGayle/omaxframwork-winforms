﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DemoHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DemoHome))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.DEMOS = New System.Windows.Forms.TabPage()
        Me.OmaxAnimatedInfoPanel4 = New OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel(Me.components)
        Me.OmaxAnimatedInfoPanel3 = New OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel(Me.components)
        Me.OmaxAnimatedInfoPanel2 = New OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel(Me.components)
        Me.OmaxAnimatedInfoPanel1 = New OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel(Me.components)
        Me.OmaxControl4 = New OmaxFrameworkDesignPattern.OmaxControl(Me.components)
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.OmaxControl3 = New OmaxFrameworkDesignPattern.OmaxControl(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.OmaxControl2 = New OmaxFrameworkDesignPattern.OmaxControl(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.OmaxControl1 = New OmaxFrameworkDesignPattern.OmaxControl(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OmaxControlHost4 = New OmaxFrameworkDesignPattern.OmaxControlHost(Me.components)
        Me.OmaxControlHost3 = New OmaxFrameworkDesignPattern.OmaxControlHost(Me.components)
        Me.OmaxControlHost2 = New OmaxFrameworkDesignPattern.OmaxControlHost(Me.components)
        Me.OmaxControlHost1 = New OmaxFrameworkDesignPattern.OmaxControlHost(Me.components)
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.CONTROLSTAB = New System.Windows.Forms.TabPage()
        Me.OmaxControl5 = New OmaxFrameworkDesignPattern.OmaxControl(Me.components)
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.NewToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.OpenToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.SaveToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.PrintToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.CopyToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.PasteToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.HelpToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.OmaxControlHost5 = New OmaxFrameworkDesignPattern.OmaxControlHost(Me.components)
        Me.OmaxGridview1 = New OmaxFrameworkDesignPattern.OmaxGridview(Me.components)
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OmaxPhotoSlider1 = New OmaxFrameworkDesignPattern.OmaxPhotoSlider(Me.components)
        Me.OmaxAnimatedInfoPanel5 = New OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel(Me.components)
        Me.OmaxCheckedButton5 = New OmaxFrameworkDesignPattern.OmaxCheckedButton(Me.components)
        Me.OmaxTextEdit1 = New OmaxFrameworkDesignPattern.OmaxTextEdit(Me.components)
        Me.UMLTAB = New System.Windows.Forms.TabPage()
        Me.NAMESPACETAB = New System.Windows.Forms.TabPage()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.OmaxCheckedButton2 = New OmaxFrameworkDesignPattern.OmaxCheckedButton(Me.components)
        Me.OmaxCheckedButton1 = New OmaxFrameworkDesignPattern.OmaxCheckedButton(Me.components)
        Me.OmaxCheckedButton4 = New OmaxFrameworkDesignPattern.OmaxCheckedButton(Me.components)
        Me.OmaxCheckedButton3 = New OmaxFrameworkDesignPattern.OmaxCheckedButton(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabControl1.SuspendLayout()
        Me.DEMOS.SuspendLayout()
        Me.OmaxControl4.SuspendLayout()
        Me.OmaxControl3.SuspendLayout()
        Me.OmaxControl2.SuspendLayout()
        Me.OmaxControl1.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTROLSTAB.SuspendLayout()
        Me.OmaxControl5.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.OmaxGridview1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OmaxPhotoSlider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NAMESPACETAB.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.DEMOS)
        Me.TabControl1.Controls.Add(Me.CONTROLSTAB)
        Me.TabControl1.Controls.Add(Me.UMLTAB)
        Me.TabControl1.Controls.Add(Me.NAMESPACETAB)
        Me.TabControl1.Location = New System.Drawing.Point(0, 69)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(924, 553)
        Me.TabControl1.TabIndex = 0
        '
        'DEMOS
        '
        Me.DEMOS.Controls.Add(Me.OmaxAnimatedInfoPanel4)
        Me.DEMOS.Controls.Add(Me.OmaxAnimatedInfoPanel3)
        Me.DEMOS.Controls.Add(Me.OmaxAnimatedInfoPanel2)
        Me.DEMOS.Controls.Add(Me.OmaxAnimatedInfoPanel1)
        Me.DEMOS.Controls.Add(Me.OmaxControl4)
        Me.DEMOS.Controls.Add(Me.OmaxControl3)
        Me.DEMOS.Controls.Add(Me.OmaxControl2)
        Me.DEMOS.Controls.Add(Me.OmaxControl1)
        Me.DEMOS.Controls.Add(Me.Label4)
        Me.DEMOS.Controls.Add(Me.Label3)
        Me.DEMOS.Controls.Add(Me.Label2)
        Me.DEMOS.Controls.Add(Me.Label1)
        Me.DEMOS.Controls.Add(Me.OmaxControlHost4)
        Me.DEMOS.Controls.Add(Me.OmaxControlHost3)
        Me.DEMOS.Controls.Add(Me.OmaxControlHost2)
        Me.DEMOS.Controls.Add(Me.OmaxControlHost1)
        Me.DEMOS.Controls.Add(Me.PictureBox5)
        Me.DEMOS.Controls.Add(Me.PictureBox4)
        Me.DEMOS.Controls.Add(Me.PictureBox3)
        Me.DEMOS.Controls.Add(Me.PictureBox2)
        Me.DEMOS.Location = New System.Drawing.Point(4, 22)
        Me.DEMOS.Name = "DEMOS"
        Me.DEMOS.Padding = New System.Windows.Forms.Padding(3)
        Me.DEMOS.Size = New System.Drawing.Size(916, 527)
        Me.DEMOS.TabIndex = 0
        Me.DEMOS.Text = "DEMO"
        Me.DEMOS.UseVisualStyleBackColor = True
        '
        'OmaxAnimatedInfoPanel4
        '
        Me.OmaxAnimatedInfoPanel4.BackColor = System.Drawing.Color.Orange
        Me.OmaxAnimatedInfoPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxAnimatedInfoPanel4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxAnimatedInfoPanel4.DescriptionFont = Nothing
        Me.OmaxAnimatedInfoPanel4.DescriptionForColor = System.Drawing.Color.Empty
        Me.OmaxAnimatedInfoPanel4.DescriptionText = "Click to run demo"
        Me.OmaxAnimatedInfoPanel4.Location = New System.Drawing.Point(183, 75)
        Me.OmaxAnimatedInfoPanel4.Name = "OmaxAnimatedInfoPanel4"
        Me.OmaxAnimatedInfoPanel4.Size = New System.Drawing.Size(207, 76)
        Me.OmaxAnimatedInfoPanel4.TabIndex = 4
        Me.OmaxAnimatedInfoPanel4.TiteForecolor = System.Drawing.Color.White
        Me.OmaxAnimatedInfoPanel4.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxAnimatedInfoPanel4.Titletext = "Run Demo"
        Me.OmaxAnimatedInfoPanel4.Visible = False
        '
        'OmaxAnimatedInfoPanel3
        '
        Me.OmaxAnimatedInfoPanel3.BackColor = System.Drawing.Color.Orange
        Me.OmaxAnimatedInfoPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxAnimatedInfoPanel3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxAnimatedInfoPanel3.DescriptionFont = Nothing
        Me.OmaxAnimatedInfoPanel3.DescriptionForColor = System.Drawing.Color.Empty
        Me.OmaxAnimatedInfoPanel3.DescriptionText = "Click to run demo"
        Me.OmaxAnimatedInfoPanel3.Location = New System.Drawing.Point(184, 342)
        Me.OmaxAnimatedInfoPanel3.Name = "OmaxAnimatedInfoPanel3"
        Me.OmaxAnimatedInfoPanel3.Size = New System.Drawing.Size(207, 76)
        Me.OmaxAnimatedInfoPanel3.TabIndex = 4
        Me.OmaxAnimatedInfoPanel3.TiteForecolor = System.Drawing.Color.White
        Me.OmaxAnimatedInfoPanel3.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxAnimatedInfoPanel3.Titletext = "Run Demo"
        Me.OmaxAnimatedInfoPanel3.Visible = False
        '
        'OmaxAnimatedInfoPanel2
        '
        Me.OmaxAnimatedInfoPanel2.BackColor = System.Drawing.Color.Orange
        Me.OmaxAnimatedInfoPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxAnimatedInfoPanel2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxAnimatedInfoPanel2.DescriptionFont = Nothing
        Me.OmaxAnimatedInfoPanel2.DescriptionForColor = System.Drawing.Color.Empty
        Me.OmaxAnimatedInfoPanel2.DescriptionText = "Click to run demo"
        Me.OmaxAnimatedInfoPanel2.Location = New System.Drawing.Point(563, 333)
        Me.OmaxAnimatedInfoPanel2.Name = "OmaxAnimatedInfoPanel2"
        Me.OmaxAnimatedInfoPanel2.Size = New System.Drawing.Size(207, 76)
        Me.OmaxAnimatedInfoPanel2.TabIndex = 4
        Me.OmaxAnimatedInfoPanel2.TiteForecolor = System.Drawing.Color.White
        Me.OmaxAnimatedInfoPanel2.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxAnimatedInfoPanel2.Titletext = "Run Demo"
        Me.OmaxAnimatedInfoPanel2.Visible = False
        '
        'OmaxAnimatedInfoPanel1
        '
        Me.OmaxAnimatedInfoPanel1.BackColor = System.Drawing.Color.Orange
        Me.OmaxAnimatedInfoPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxAnimatedInfoPanel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxAnimatedInfoPanel1.DescriptionFont = Nothing
        Me.OmaxAnimatedInfoPanel1.DescriptionForColor = System.Drawing.Color.Empty
        Me.OmaxAnimatedInfoPanel1.DescriptionText = "Click to run demo"
        Me.OmaxAnimatedInfoPanel1.Location = New System.Drawing.Point(563, 81)
        Me.OmaxAnimatedInfoPanel1.Name = "OmaxAnimatedInfoPanel1"
        Me.OmaxAnimatedInfoPanel1.Size = New System.Drawing.Size(207, 76)
        Me.OmaxAnimatedInfoPanel1.TabIndex = 4
        Me.OmaxAnimatedInfoPanel1.TiteForecolor = System.Drawing.Color.White
        Me.OmaxAnimatedInfoPanel1.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxAnimatedInfoPanel1.Titletext = "Run Demo"
        Me.OmaxAnimatedInfoPanel1.Visible = False
        '
        'OmaxControl4
        '
        Me.OmaxControl4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxControl4.Controls.Add(Me.Label12)
        Me.OmaxControl4.Controls.Add(Me.Label11)
        Me.OmaxControl4.ForeColor = System.Drawing.Color.White
        Me.OmaxControl4.Location = New System.Drawing.Point(292, 222)
        Me.OmaxControl4.Name = "OmaxControl4"
        Me.OmaxControl4.Size = New System.Drawing.Size(261, 260)
        Me.OmaxControl4.TabIndex = 3
        Me.OmaxControl4.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(4, 38)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(172, 208)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = resources.GetString("Label12.Text")
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label11.Location = New System.Drawing.Point(8, 7)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(234, 20)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Omax Ecommerce Shopping"
        '
        'OmaxControl3
        '
        Me.OmaxControl3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxControl3.Controls.Add(Me.Label9)
        Me.OmaxControl3.Controls.Add(Me.Label10)
        Me.OmaxControl3.ForeColor = System.Drawing.Color.White
        Me.OmaxControl3.Location = New System.Drawing.Point(619, 188)
        Me.OmaxControl3.Name = "OmaxControl3"
        Me.OmaxControl3.Size = New System.Drawing.Size(261, 159)
        Me.OmaxControl3.TabIndex = 3
        Me.OmaxControl3.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label9.Location = New System.Drawing.Point(8, 10)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(237, 20)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Omax Information Scrambler"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 38)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(203, 65)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "This is an information scrambling" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Control that uses OmaxInformationHiding" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "to sw" & _
    "ap out words with random letters" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and then converts the random generated " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "lette" & _
    "rs back to the original words"
        '
        'OmaxControl2
        '
        Me.OmaxControl2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxControl2.Controls.Add(Me.Label7)
        Me.OmaxControl2.Controls.Add(Me.Label8)
        Me.OmaxControl2.ForeColor = System.Drawing.Color.White
        Me.OmaxControl2.Location = New System.Drawing.Point(419, 65)
        Me.OmaxControl2.Name = "OmaxControl2"
        Me.OmaxControl2.Size = New System.Drawing.Size(261, 175)
        Me.OmaxControl2.TabIndex = 3
        Me.OmaxControl2.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label7.Location = New System.Drawing.Point(60, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(131, 20)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Omax Financial"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(4, 38)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(221, 78)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = resources.GetString("Label8.Text")
        '
        'OmaxControl1
        '
        Me.OmaxControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxControl1.Controls.Add(Me.Label6)
        Me.OmaxControl1.Controls.Add(Me.Label5)
        Me.OmaxControl1.ForeColor = System.Drawing.Color.White
        Me.OmaxControl1.Location = New System.Drawing.Point(8, 6)
        Me.OmaxControl1.Name = "OmaxControl1"
        Me.OmaxControl1.Size = New System.Drawing.Size(261, 269)
        Me.OmaxControl1.TabIndex = 3
        Me.OmaxControl1.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label6.Location = New System.Drawing.Point(19, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(212, 20)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Omax Server ImageSlider"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(4, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(241, 208)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = resources.GetString("Label5.Text")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label4.Location = New System.Drawing.Point(180, 255)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(211, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Omax Ecommerce Shopping"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label3.Location = New System.Drawing.Point(561, 255)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(211, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Omax Information Scrambler"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(605, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(117, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Omax Financial"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(191, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(189, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Omax Server ImageSlider"
        '
        'OmaxControlHost4
        '
        Me.OmaxControlHost4.BackColor = System.Drawing.Color.SteelBlue
        Me.OmaxControlHost4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControlHost4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxControlHost4.ForeColor = System.Drawing.Color.White
        Me.OmaxControlHost4.HostedControl = Me.OmaxControl4
        Me.OmaxControlHost4.Location = New System.Drawing.Point(180, 457)
        Me.OmaxControlHost4.Name = "OmaxControlHost4"
        Me.OmaxControlHost4.Size = New System.Drawing.Size(215, 33)
        Me.OmaxControlHost4.TabIndex = 1
        Me.OmaxControlHost4.Text = "View info"
        Me.OmaxControlHost4.ToolTipText = ""
        Me.OmaxControlHost4.UseVisualStyleBackColor = False
        '
        'OmaxControlHost3
        '
        Me.OmaxControlHost3.BackColor = System.Drawing.Color.SteelBlue
        Me.OmaxControlHost3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControlHost3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxControlHost3.ForeColor = System.Drawing.Color.White
        Me.OmaxControlHost3.HostedControl = Me.OmaxControl3
        Me.OmaxControlHost3.Location = New System.Drawing.Point(559, 457)
        Me.OmaxControlHost3.Name = "OmaxControlHost3"
        Me.OmaxControlHost3.Size = New System.Drawing.Size(215, 33)
        Me.OmaxControlHost3.TabIndex = 1
        Me.OmaxControlHost3.Text = "View info"
        Me.OmaxControlHost3.ToolTipText = ""
        Me.OmaxControlHost3.UseVisualStyleBackColor = False
        '
        'OmaxControlHost2
        '
        Me.OmaxControlHost2.BackColor = System.Drawing.Color.SteelBlue
        Me.OmaxControlHost2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControlHost2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxControlHost2.ForeColor = System.Drawing.Color.White
        Me.OmaxControlHost2.HostedControl = Me.OmaxControl2
        Me.OmaxControlHost2.Location = New System.Drawing.Point(559, 207)
        Me.OmaxControlHost2.Name = "OmaxControlHost2"
        Me.OmaxControlHost2.Size = New System.Drawing.Size(215, 33)
        Me.OmaxControlHost2.TabIndex = 1
        Me.OmaxControlHost2.Text = "View info"
        Me.OmaxControlHost2.ToolTipText = ""
        Me.OmaxControlHost2.UseVisualStyleBackColor = False
        '
        'OmaxControlHost1
        '
        Me.OmaxControlHost1.BackColor = System.Drawing.Color.SteelBlue
        Me.OmaxControlHost1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxControlHost1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxControlHost1.ForeColor = System.Drawing.Color.White
        Me.OmaxControlHost1.HostedControl = Me.OmaxControl1
        Me.OmaxControlHost1.Location = New System.Drawing.Point(180, 207)
        Me.OmaxControlHost1.Name = "OmaxControlHost1"
        Me.OmaxControlHost1.Size = New System.Drawing.Size(215, 33)
        Me.OmaxControlHost1.TabIndex = 1
        Me.OmaxControlHost1.Text = "View info"
        Me.OmaxControlHost1.ToolTipText = ""
        Me.OmaxControlHost1.UseVisualStyleBackColor = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox5.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.EcommerceShoppingDemo
        Me.PictureBox5.Location = New System.Drawing.Point(180, 278)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(215, 180)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 0
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox4.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.InformationScramblerdemo
        Me.PictureBox4.Location = New System.Drawing.Point(559, 278)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(215, 180)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox3.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.FInancialDemo
        Me.PictureBox3.Location = New System.Drawing.Point(559, 28)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(215, 180)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.ServerImageSliderDemo
        Me.PictureBox2.Location = New System.Drawing.Point(180, 28)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(215, 180)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'CONTROLSTAB
        '
        Me.CONTROLSTAB.Controls.Add(Me.OmaxControl5)
        Me.CONTROLSTAB.Controls.Add(Me.Label18)
        Me.CONTROLSTAB.Controls.Add(Me.Label17)
        Me.CONTROLSTAB.Controls.Add(Me.Label16)
        Me.CONTROLSTAB.Controls.Add(Me.Label15)
        Me.CONTROLSTAB.Controls.Add(Me.Label14)
        Me.CONTROLSTAB.Controls.Add(Me.Label13)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxControlHost5)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxGridview1)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxPhotoSlider1)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxAnimatedInfoPanel5)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxCheckedButton5)
        Me.CONTROLSTAB.Controls.Add(Me.OmaxTextEdit1)
        Me.CONTROLSTAB.Location = New System.Drawing.Point(4, 22)
        Me.CONTROLSTAB.Name = "CONTROLSTAB"
        Me.CONTROLSTAB.Padding = New System.Windows.Forms.Padding(3)
        Me.CONTROLSTAB.Size = New System.Drawing.Size(916, 527)
        Me.CONTROLSTAB.TabIndex = 1
        Me.CONTROLSTAB.Text = "CONTROLS"
        Me.CONTROLSTAB.UseVisualStyleBackColor = True
        '
        'OmaxControl5
        '
        Me.OmaxControl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxControl5.Controls.Add(Me.TabControl2)
        Me.OmaxControl5.Controls.Add(Me.ToolStrip1)
        Me.OmaxControl5.Location = New System.Drawing.Point(445, 304)
        Me.OmaxControl5.Name = "OmaxControl5"
        Me.OmaxControl5.Size = New System.Drawing.Size(390, 272)
        Me.OmaxControl5.TabIndex = 6
        Me.OmaxControl5.Visible = False
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage1)
        Me.TabControl2.Controls.Add(Me.TabPage2)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.Location = New System.Drawing.Point(0, 25)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(388, 245)
        Me.TabControl2.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.DateTimePicker1)
        Me.TabPage1.Controls.Add(Me.PictureBox6)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(380, 219)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(179, 124)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(94, 43)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Button1"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(164, 71)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(174, 43)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "I can Host any Control :)"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(138, 18)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 1
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.OMAXICO_LOGIN
        Me.PictureBox6.Location = New System.Drawing.Point(19, 36)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(104, 65)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(380, 219)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripButton, Me.OpenToolStripButton, Me.SaveToolStripButton, Me.PrintToolStripButton, Me.toolStripSeparator, Me.CutToolStripButton, Me.CopyToolStripButton, Me.PasteToolStripButton, Me.toolStripSeparator1, Me.HelpToolStripButton})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(388, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'NewToolStripButton
        '
        Me.NewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.NewToolStripButton.Image = CType(resources.GetObject("NewToolStripButton.Image"), System.Drawing.Image)
        Me.NewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewToolStripButton.Name = "NewToolStripButton"
        Me.NewToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.NewToolStripButton.Text = "&New"
        '
        'OpenToolStripButton
        '
        Me.OpenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.OpenToolStripButton.Image = CType(resources.GetObject("OpenToolStripButton.Image"), System.Drawing.Image)
        Me.OpenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenToolStripButton.Name = "OpenToolStripButton"
        Me.OpenToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.OpenToolStripButton.Text = "&Open"
        '
        'SaveToolStripButton
        '
        Me.SaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.SaveToolStripButton.Image = CType(resources.GetObject("SaveToolStripButton.Image"), System.Drawing.Image)
        Me.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripButton.Name = "SaveToolStripButton"
        Me.SaveToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.SaveToolStripButton.Text = "&Save"
        '
        'PrintToolStripButton
        '
        Me.PrintToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PrintToolStripButton.Image = CType(resources.GetObject("PrintToolStripButton.Image"), System.Drawing.Image)
        Me.PrintToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintToolStripButton.Name = "PrintToolStripButton"
        Me.PrintToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.PrintToolStripButton.Text = "&Print"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'CutToolStripButton
        '
        Me.CutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CutToolStripButton.Image = CType(resources.GetObject("CutToolStripButton.Image"), System.Drawing.Image)
        Me.CutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CutToolStripButton.Name = "CutToolStripButton"
        Me.CutToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.CutToolStripButton.Text = "C&ut"
        '
        'CopyToolStripButton
        '
        Me.CopyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CopyToolStripButton.Image = CType(resources.GetObject("CopyToolStripButton.Image"), System.Drawing.Image)
        Me.CopyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CopyToolStripButton.Name = "CopyToolStripButton"
        Me.CopyToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.CopyToolStripButton.Text = "&Copy"
        '
        'PasteToolStripButton
        '
        Me.PasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PasteToolStripButton.Image = CType(resources.GetObject("PasteToolStripButton.Image"), System.Drawing.Image)
        Me.PasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PasteToolStripButton.Name = "PasteToolStripButton"
        Me.PasteToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.PasteToolStripButton.Text = "&Paste"
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'HelpToolStripButton
        '
        Me.HelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.HelpToolStripButton.Image = CType(resources.GetObject("HelpToolStripButton.Image"), System.Drawing.Image)
        Me.HelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.HelpToolStripButton.Name = "HelpToolStripButton"
        Me.HelpToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.HelpToolStripButton.Text = "He&lp"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(303, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(357, 13)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "A Gridview with MultiRow adding,Deleteing,Rowvalues Summary and Print"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(303, 162)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(258, 13)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "A control that Host other Controls in a Drop down box"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(26, 155)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(122, 13)
        Me.Label16.TabIndex = 10
        Me.Label16.Text = "Animated Loading Panel"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(26, 304)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(175, 13)
        Me.Label15.TabIndex = 9
        Me.Label15.Text = "Slides Images from a Server Source"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(26, 90)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(170, 13)
        Me.Label14.TabIndex = 8
        Me.Label14.Text = "A button that acts like a Checkbox"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(26, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(206, 13)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "TextEdit Control. Returns Value from string"
        '
        'OmaxControlHost5
        '
        Me.OmaxControlHost5.HostedControl = Me.OmaxControl5
        Me.OmaxControlHost5.Location = New System.Drawing.Point(306, 178)
        Me.OmaxControlHost5.Name = "OmaxControlHost5"
        Me.OmaxControlHost5.Size = New System.Drawing.Size(200, 37)
        Me.OmaxControlHost5.TabIndex = 5
        Me.OmaxControlHost5.Text = "OmaxControlHost5"
        Me.OmaxControlHost5.ToolTipText = "Click to view Control"
        Me.OmaxControlHost5.UseVisualStyleBackColor = True
        '
        'OmaxGridview1
        '
        Me.OmaxGridview1.BackgroundColor = System.Drawing.Color.White
        Me.OmaxGridview1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.OmaxGridview1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.OmaxGridview1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.OmaxGridview1.Location = New System.Drawing.Point(306, 37)
        Me.OmaxGridview1.Name = "OmaxGridview1"
        Me.OmaxGridview1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.OmaxGridview1.RowHeadersVisible = False
        Me.OmaxGridview1.Size = New System.Drawing.Size(254, 94)
        Me.OmaxGridview1.TabIndex = 4
        '
        'Column1
        '
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 97
        '
        'Column2
        '
        Me.Column2.HeaderText = "Column2"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 97
        '
        'OmaxPhotoSlider1
        '
        Me.OmaxPhotoSlider1.Command = Nothing
        Me.OmaxPhotoSlider1.ConnectionString = Nothing
        Me.OmaxPhotoSlider1.DataBaseType = OmaxFrameworkDesignPattern.OmaxFramework.OmaxData.DataBaseType.Access
        Me.OmaxPhotoSlider1.DataSource = Nothing
        Me.OmaxPhotoSlider1.EnableAutoSlide = False
        Me.OmaxPhotoSlider1.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.Fruits_Wallpaper_91
        Me.OmaxPhotoSlider1.Images = Nothing
        Me.OmaxPhotoSlider1.Interval = 10
        Me.OmaxPhotoSlider1.Location = New System.Drawing.Point(29, 322)
        Me.OmaxPhotoSlider1.LoopImages = False
        Me.OmaxPhotoSlider1.Name = "OmaxPhotoSlider1"
        Me.OmaxPhotoSlider1.Size = New System.Drawing.Size(151, 100)
        Me.OmaxPhotoSlider1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.OmaxPhotoSlider1.TabIndex = 3
        Me.OmaxPhotoSlider1.TabStop = False
        '
        'OmaxAnimatedInfoPanel5
        '
        Me.OmaxAnimatedInfoPanel5.DescriptionFont = Nothing
        Me.OmaxAnimatedInfoPanel5.DescriptionForColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxAnimatedInfoPanel5.DescriptionText = "My Description"
        Me.OmaxAnimatedInfoPanel5.Location = New System.Drawing.Point(29, 178)
        Me.OmaxAnimatedInfoPanel5.Name = "OmaxAnimatedInfoPanel5"
        Me.OmaxAnimatedInfoPanel5.Size = New System.Drawing.Size(200, 72)
        Me.OmaxAnimatedInfoPanel5.TabIndex = 2
        Me.OmaxAnimatedInfoPanel5.TiteForecolor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.OmaxAnimatedInfoPanel5.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxAnimatedInfoPanel5.Titletext = "My Title"
        '
        'OmaxCheckedButton5
        '
        Me.OmaxCheckedButton5.CheckBackColor = System.Drawing.Color.Yellow
        Me.OmaxCheckedButton5.CheckSate = System.Windows.Forms.CheckState.Checked
        Me.OmaxCheckedButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxCheckedButton5.Location = New System.Drawing.Point(29, 108)
        Me.OmaxCheckedButton5.Name = "OmaxCheckedButton5"
        Me.OmaxCheckedButton5.Size = New System.Drawing.Size(151, 23)
        Me.OmaxCheckedButton5.TabIndex = 1
        Me.OmaxCheckedButton5.Text = "OmaxCheckedButton5"
        Me.OmaxCheckedButton5.UnCheckBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton5.UseVisualStyleBackColor = True
        '
        'OmaxTextEdit1
        '
        Me.OmaxTextEdit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OmaxTextEdit1.Location = New System.Drawing.Point(29, 37)
        Me.OmaxTextEdit1.Multiline = True
        Me.OmaxTextEdit1.Name = "OmaxTextEdit1"
        Me.OmaxTextEdit1.NullValue = Nothing
        Me.OmaxTextEdit1.Size = New System.Drawing.Size(151, 24)
        Me.OmaxTextEdit1.TabIndex = 0
        Me.OmaxTextEdit1.Text = "OmaxTextEdit"
        '
        'UMLTAB
        '
        Me.UMLTAB.Location = New System.Drawing.Point(4, 22)
        Me.UMLTAB.Name = "UMLTAB"
        Me.UMLTAB.Padding = New System.Windows.Forms.Padding(3)
        Me.UMLTAB.Size = New System.Drawing.Size(916, 527)
        Me.UMLTAB.TabIndex = 2
        Me.UMLTAB.Text = "UML"
        Me.UMLTAB.UseVisualStyleBackColor = True
        '
        'NAMESPACETAB
        '
        Me.NAMESPACETAB.Controls.Add(Me.Label24)
        Me.NAMESPACETAB.Controls.Add(Me.Label23)
        Me.NAMESPACETAB.Controls.Add(Me.Label22)
        Me.NAMESPACETAB.Controls.Add(Me.Label21)
        Me.NAMESPACETAB.Controls.Add(Me.Label20)
        Me.NAMESPACETAB.Controls.Add(Me.Label19)
        Me.NAMESPACETAB.Location = New System.Drawing.Point(4, 22)
        Me.NAMESPACETAB.Name = "NAMESPACETAB"
        Me.NAMESPACETAB.Padding = New System.Windows.Forms.Padding(3)
        Me.NAMESPACETAB.Size = New System.Drawing.Size(916, 527)
        Me.NAMESPACETAB.TabIndex = 3
        Me.NAMESPACETAB.Text = "NAMESPACE"
        Me.NAMESPACETAB.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label24.Location = New System.Drawing.Point(21, 258)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(111, 26)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "OmaxMail"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label23.Location = New System.Drawing.Point(21, 214)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(204, 26)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "OmaxDateAndTime"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label22.Location = New System.Drawing.Point(21, 168)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(242, 26)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "OmaxInformationHiding"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label21.Location = New System.Drawing.Point(21, 122)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(132, 26)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "OmaxImage"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label20.Location = New System.Drawing.Point(21, 76)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(158, 26)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "OmaxFinancial"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label19.Location = New System.Drawing.Point(21, 30)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(117, 26)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "OmaxData"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(924, 91)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel2.Controls.Add(Me.OmaxCheckedButton2)
        Me.Panel2.Controls.Add(Me.OmaxCheckedButton1)
        Me.Panel2.Controls.Add(Me.OmaxCheckedButton4)
        Me.Panel2.Controls.Add(Me.OmaxCheckedButton3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(77, 46)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(847, 45)
        Me.Panel2.TabIndex = 1
        '
        'OmaxCheckedButton2
        '
        Me.OmaxCheckedButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton2.CheckBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton2.CheckSate = System.Windows.Forms.CheckState.Unchecked
        Me.OmaxCheckedButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxCheckedButton2.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxCheckedButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxCheckedButton2.ForeColor = System.Drawing.Color.White
        Me.OmaxCheckedButton2.Location = New System.Drawing.Point(143, 8)
        Me.OmaxCheckedButton2.Name = "OmaxCheckedButton2"
        Me.OmaxCheckedButton2.Size = New System.Drawing.Size(139, 38)
        Me.OmaxCheckedButton2.TabIndex = 2
        Me.OmaxCheckedButton2.Text = "CONTROLS"
        Me.OmaxCheckedButton2.UnCheckBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton2.UseVisualStyleBackColor = False
        '
        'OmaxCheckedButton1
        '
        Me.OmaxCheckedButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton1.CheckBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton1.CheckSate = System.Windows.Forms.CheckState.Unchecked
        Me.OmaxCheckedButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxCheckedButton1.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxCheckedButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxCheckedButton1.ForeColor = System.Drawing.Color.White
        Me.OmaxCheckedButton1.Location = New System.Drawing.Point(3, 8)
        Me.OmaxCheckedButton1.Name = "OmaxCheckedButton1"
        Me.OmaxCheckedButton1.Size = New System.Drawing.Size(139, 38)
        Me.OmaxCheckedButton1.TabIndex = 2
        Me.OmaxCheckedButton1.Text = "DEMO"
        Me.OmaxCheckedButton1.UnCheckBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton1.UseVisualStyleBackColor = False
        '
        'OmaxCheckedButton4
        '
        Me.OmaxCheckedButton4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton4.CheckBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton4.CheckSate = System.Windows.Forms.CheckState.Unchecked
        Me.OmaxCheckedButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxCheckedButton4.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxCheckedButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxCheckedButton4.ForeColor = System.Drawing.Color.White
        Me.OmaxCheckedButton4.Location = New System.Drawing.Point(423, 8)
        Me.OmaxCheckedButton4.Name = "OmaxCheckedButton4"
        Me.OmaxCheckedButton4.Size = New System.Drawing.Size(139, 38)
        Me.OmaxCheckedButton4.TabIndex = 2
        Me.OmaxCheckedButton4.Text = "NAMESPACE"
        Me.OmaxCheckedButton4.UnCheckBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton4.UseVisualStyleBackColor = False
        '
        'OmaxCheckedButton3
        '
        Me.OmaxCheckedButton3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton3.CheckBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton3.CheckSate = System.Windows.Forms.CheckState.Unchecked
        Me.OmaxCheckedButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OmaxCheckedButton3.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue
        Me.OmaxCheckedButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OmaxCheckedButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OmaxCheckedButton3.ForeColor = System.Drawing.Color.White
        Me.OmaxCheckedButton3.Location = New System.Drawing.Point(283, 8)
        Me.OmaxCheckedButton3.Name = "OmaxCheckedButton3"
        Me.OmaxCheckedButton3.Size = New System.Drawing.Size(139, 38)
        Me.OmaxCheckedButton3.TabIndex = 2
        Me.OmaxCheckedButton3.Text = "UML"
        Me.OmaxCheckedButton3.UnCheckBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OmaxCheckedButton3.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = Global.OmaxFrameworkDesignPattern.My.Resources.Resources.OMAXICO_LOGIN
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 91)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'DemoHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 622)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(940, 661)
        Me.Name = "DemoHome"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DemoHome"
        Me.TabControl1.ResumeLayout(False)
        Me.DEMOS.ResumeLayout(False)
        Me.DEMOS.PerformLayout()
        Me.OmaxControl4.ResumeLayout(False)
        Me.OmaxControl4.PerformLayout()
        Me.OmaxControl3.ResumeLayout(False)
        Me.OmaxControl3.PerformLayout()
        Me.OmaxControl2.ResumeLayout(False)
        Me.OmaxControl2.PerformLayout()
        Me.OmaxControl1.ResumeLayout(False)
        Me.OmaxControl1.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTROLSTAB.ResumeLayout(False)
        Me.CONTROLSTAB.PerformLayout()
        Me.OmaxControl5.ResumeLayout(False)
        Me.OmaxControl5.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.OmaxGridview1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OmaxPhotoSlider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NAMESPACETAB.ResumeLayout(False)
        Me.NAMESPACETAB.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents DEMOS As System.Windows.Forms.TabPage
    Friend WithEvents CONTROLSTAB As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents UMLTAB As System.Windows.Forms.TabPage
    Friend WithEvents NAMESPACETAB As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents OmaxCheckedButton2 As OmaxFrameworkDesignPattern.OmaxCheckedButton
    Friend WithEvents OmaxCheckedButton1 As OmaxFrameworkDesignPattern.OmaxCheckedButton
    Friend WithEvents OmaxCheckedButton4 As OmaxFrameworkDesignPattern.OmaxCheckedButton
    Friend WithEvents OmaxCheckedButton3 As OmaxFrameworkDesignPattern.OmaxCheckedButton
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents OmaxControl1 As OmaxFrameworkDesignPattern.OmaxControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OmaxControlHost4 As OmaxFrameworkDesignPattern.OmaxControlHost
    Friend WithEvents OmaxControlHost3 As OmaxFrameworkDesignPattern.OmaxControlHost
    Friend WithEvents OmaxControlHost2 As OmaxFrameworkDesignPattern.OmaxControlHost
    Friend WithEvents OmaxControlHost1 As OmaxFrameworkDesignPattern.OmaxControlHost
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents OmaxControl4 As OmaxFrameworkDesignPattern.OmaxControl
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents OmaxControl3 As OmaxFrameworkDesignPattern.OmaxControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents OmaxControl2 As OmaxFrameworkDesignPattern.OmaxControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents OmaxAnimatedInfoPanel1 As OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel
    Friend WithEvents OmaxAnimatedInfoPanel4 As OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel
    Friend WithEvents OmaxAnimatedInfoPanel3 As OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel
    Friend WithEvents OmaxAnimatedInfoPanel2 As OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel
    Friend WithEvents OmaxControl5 As OmaxFrameworkDesignPattern.OmaxControl
    Friend WithEvents OmaxControlHost5 As OmaxFrameworkDesignPattern.OmaxControlHost
    Friend WithEvents OmaxGridview1 As OmaxFrameworkDesignPattern.OmaxGridview
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OmaxPhotoSlider1 As OmaxFrameworkDesignPattern.OmaxPhotoSlider
    Friend WithEvents OmaxAnimatedInfoPanel5 As OmaxFrameworkDesignPattern.OmaxAnimatedInfoPanel
    Friend WithEvents OmaxCheckedButton5 As OmaxFrameworkDesignPattern.OmaxCheckedButton
    Friend WithEvents OmaxTextEdit1 As OmaxFrameworkDesignPattern.OmaxTextEdit
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents NewToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents OpenToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents SaveToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents PrintToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents CopyToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents PasteToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents HelpToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
