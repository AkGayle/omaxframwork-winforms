﻿Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxData
Imports OmaxFrameworkDesignPattern.EcomHandler
Public Class Customers
    Private Sub Customers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OmaxGridview1.DataSource = GetAllCustomer()
            OmaxGridview1.BestFitAllRowsAndColumns()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim shopp As New SHoppingCart
            shopp.Show()
            shopp.LoadCustomer(OmaxGridview1.GetSelectedRowCellDisplayText("CustomerID"))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Try
            OmaxGridview1.DataSource = FilterCustomers(TextBox1.Text)
        Catch ex As Exception

        End Try
    End Sub
End Class