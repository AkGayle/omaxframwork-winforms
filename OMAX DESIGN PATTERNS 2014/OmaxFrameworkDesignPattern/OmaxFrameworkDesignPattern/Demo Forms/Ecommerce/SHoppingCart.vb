﻿Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxData
Imports OmaxFrameworkDesignPattern.EcomHandler
Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxImage
Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxDateAndTime
Imports OmaxFrameworkDesignPattern.OmaxFramework.Financial
Public Class SHoppingCart
    Private CustomerIDs As Integer


    ''' <summary>
    ''' Loads the selected for purchase
    ''' </summary>
    ''' <param name="CustomerID"> The Customer's ID</param>
    ''' <remarks></remarks>
    Public Sub LoadCustomer(ByVal CustomerID As Integer)
        CustomerIDs = CustomerID
        Dim CustomerList As DataRowView
        Try
            Dim Bsource As New BindingSource
            Bsource = GetCustomer(CustomerID)

            For Each CustomerList In Bsource.List
                If IsNothing(CustomerList.Item("Photo")) = True Then
                    'do nothing
                Else
                    PictureBox1.Image = Image.FromStream(GetPicture(CustomerList.Item("Photo")))
                End If
                NameLabel.Text = CustomerList.Item("MiddleName").ToString
                IdLabel.Text = CustomerList.Item("CustomerID").ToString
                DateLebel.Text = CurrentDate.ToShortDateString
            Next
            DiscountPercentLabel.Text = (CType(GetCustomerDiscountLevel(CustomerID), Decimal) / 100).ToString("p0")
            RefreshTotal()
        Catch ex As Exception
            Try
                NameLabel.Text = CustomerList.Item("MiddleName").ToString
                IdLabel.Text = CustomerList.Item("CustomerID").ToString
                DateLebel.Text = CurrentDate.ToShortDateString
                DiscountPercentLabel.Text = (CType(GetCustomerDiscountLevel(CustomerID), Decimal) / 100).ToString("p0")
                RefreshTotal()
            Catch exs As Exception

            End Try
        End Try

    End Sub
    ''' <summary>
    ''' Adds Item to customers cart
    ''' </summary>
    ''' <param name="ProductIDs">Id of the Product from Inventory</param>
    ''' <remarks></remarks>
    Private Sub AddToCart(ByVal ProductIDs As Integer)
        Try
            If CustomerIDs > 0 Then
                AddItemToCart(CustomerIDs, 1, ProductIDs _
                                   , OmaxTextEdit1.EditValue _
                                   , OmaxGridview2.GetSelectedRowCellDisplayText("Description") _
                                   , Val(OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")) _
                                   , Val(OmaxGridview2.GetSelectedRowCellDisplayText("CostPrice")) _
                                   , Val(GetPercentageValue((((OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")) _
                                     - GetPercentageValue(Val(OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")), _
                                     GetCustomerDiscountLevel(CustomerIDs))) * Val(OmaxTextEdit1.EditValue)), _
                         OmaxGridview2.GetSelectedRowCellDisplayText("TaxPercent"))) _
                                   , Val(AddPercentage(((OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")) _
                                    - GetPercentageValue(Val(OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")), _
                                                         GetCustomerDiscountLevel(CustomerIDs))) _
                                                       * OmaxTextEdit1.EditValue, OmaxGridview2.GetSelectedRowCellDisplayText("TaxPercent"))) _
                                   , Val(OmaxGridview2.GetSelectedRowCellDisplayText("TaxPercent")) _
                                   , (GetPercentageValue(Val(OmaxGridview2.GetSelectedRowCellDisplayText("SellingPrice")), _
                                                         GetCustomerDiscountLevel(CustomerIDs))) * Val(OmaxTextEdit1.EditValue))

                RefreshTotal()
            Else

                MsgBox("Must select customer first")
                Button5.PerformClick()
            End If

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Updates Customer's cart info
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RefreshTotal()
        BillNumberLabel.Text = ReciptNumber
        BillNumberLabel1.Text = ReciptNumber
        OmaxGridview1.DataSource = GetCartItems(CustomerIDs)
        TotalDueLabel.Text = Val(OmaxGridview1.GetColumnSumaryText("LineTotal")).ToString("c2")
        TotalDueLabel1.Text = Val(OmaxGridview1.GetColumnSumaryText("LineTotal")).ToString("c2")
        DiscountLabel.Text = Val(OmaxGridview1.GetColumnSumaryText("Discount")).ToString("c2")
        DiscountLabel1.Text = Val(OmaxGridview1.GetColumnSumaryText("Discount")).ToString("c2")
        OmaxGridview1.BestFitAllRowsAndColumns()
    End Sub

    ''' <summary>
    ''' Shows checkout form with bill summary and print options
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowCheckOutHandler()
        Dim ChkHdle As New CheckOutForm
        ChkHdle.TotalDueLabel1.Text = Val(OmaxGridview1.GetColumnSumaryText("LineTotal")).ToString("c2")
        ChkHdle.DiscountLabel.Text = Val(OmaxGridview1.GetColumnSumaryText("Discount")).ToString("c2")
        ChkHdle.CustomerNameLabel.Text = NameLabel.Text
        ChkHdle.BillNumberLabel1.Text = BillNumberLabel.Text
        ChkHdle.PaymentTypeLabel.Text = ComboBox1.Text
        ChkHdle.TenderedLabel.Text = Val(TextBox1.Text).ToString("c2")
        ChkHdle.ChangeLabel.Text = ChangeLabel.Text
        ChkHdle.TaxLabel.Text = Val(OmaxGridview1.GetSelectedRowCellDisplayText("LineTax")).ToString("c2")
        ChkHdle.ShowDialog()
    End Sub

    ''' <summary>
    ''' Adds Selected Products to Customer's Cart
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AddSelectedRows()
        Try
            If CustomerIDs > 0 Then
                AddSelectedProducts(CustomerIDs, OmaxTextEdit1.EditValue, OmaxGridview2.GetSelectedRowCellValues("ProductID"))
                RefreshTotal()
            Else
                MsgBox("Must select customer first")
                Button5.PerformClick()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click, OmaxGridview2.DoubleClick, Button7.Click
        If OmaxGridview2.SelectedRows.Count > 1 Then
            AddSelectedRows()
        Else
            AddToCart(Val(OmaxGridview2.GetSelectedRowCellDisplayText("ProductID")))
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim PaymentOptions As PaymentTypes
            Select Case ComboBox1.Text
                Case "Cash"
                    PaymentOptions = PaymentTypes.Cash
                Case "Credit Card"
                    PaymentOptions = PaymentTypes.CreditCard
                Case "Debit Card"
                    PaymentOptions = PaymentTypes.DebitCard
                Case "Checque"
                    PaymentOptions = PaymentTypes.Cheque
            End Select

            'Performs Checkout
            If Val(OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal")) > 0 AndAlso Val(TextBox1.Text) _
                >= Val(OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal")) Then
                CheckOut(CustomerIDs, Val(OmaxGridview1.GetColumnSumaryText("LineTotal")) _
                                           , Val(OmaxGridview1.GetColumnSumaryText("LineTax")), Val(OmaxGridview1.GetColumnSumaryText("Discount")) _
                                           , Val(TextBox1.Text), Val(OmaxGridview1.GetColumnSumaryText("LineTotal")) - Val(TextBox1.Text), PaymentOptions)

                'Displays the checkout Form
                ShowCheckOutHandler()

                'Refreshes the window for a new sale
                BillNumberLabel.Text = 0
                BillNumberLabel1.Text = 0
                TotalDueLabel.Text = 0
                TotalDueLabel1.Text = 0
                ChangeLabel.Text = 0
                OmaxTextEdit1.Text = 0
                TextBox1.Text = 0
                DiscountLabel.Text = 0
                DiscountLabel1.Text = 0
                OmaxGridview1.DataSource = GetCartItems(0)
                RefreshInventory()
                Me.Close()
            ElseIf OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal") <= 0 Then
                MsgBox("Must +Add items to cart first")
            ElseIf Val(TextBox1.Text) <= Val(OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal")) Then
                MsgBox("PLEASE ENTER CORRECT AMOUNT")
            End If

        Catch ex As Exception
            ' MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs)
        Try
            Customers.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub PrintBill()
        OmaxGridview1.Print()
    End Sub
    Public Sub PreviewBill()
        OmaxGridview1.ShowPrintPreview()
    End Sub

    Private Sub RefreshInventory()
        Try
            Try
                OmaxGridview2.DataSource = GetProductList()
                For Each CostPrice As DataRowView In OmaxGridview2.DataSource
                    If CostPrice.Item("CostPrice").ToString = String.Empty Then
                        CostPrice.Item("CostPrice") = 0
                    End If
                Next
                ComboBox1.SelectedIndex = 0
                OmaxGridview1.BestFitAllRowsAndColumns()
                OmaxGridview2.BestFitAllRowsAndColumns()
            Catch ex As Exception

            End Try
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SHoppingCart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OmaxGridview2.DataSource = GetProductList()
            For Each CostPrice As DataRowView In OmaxGridview2.DataSource
                If CostPrice.Item("CostPrice").ToString = String.Empty Then
                    CostPrice.Item("CostPrice") = 0
                End If
            Next
            ComboBox1.SelectedIndex = 0
            OmaxGridview1.BestFitAllRowsAndColumns()
            OmaxGridview2.BestFitAllRowsAndColumns()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim ask As DialogResult = MsgBox("Are you sure you wan to cancel sale?", MsgBoxStyle.YesNo)

            If OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal") > 0 Then
                If ask = Windows.Forms.DialogResult.Yes Then
                    OmaxGridview1.DataSource = CancelSale()
                    RefreshTotal()
                    RefreshInventory()
                End If

            Else
                MsgBox("Must +Add items to cart first")
            End If
        Catch ex As Exception

        End Try

    End Sub

  
    Private Sub PictureBox1_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox1.Paint
        Try
            If IsNothing(PictureBox1.Image) = True Then
                PictureBox1.Image = My.Resources.Fruits_Wallpaper_91
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private BillTotal As Integer

    Private Sub TextBox1_Enter(sender As Object, e As EventArgs) Handles TextBox1.Enter
        BillTotal = (Val(OmaxGridview1.GetSelectedRowCellDisplayText("LineTotal")) - Val(TextBox1.Text))
    End Sub
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Try
            If CustomerIDs > 0 Then
                If ((Val(TotalDueLabel.Text.Replace("$", "").Replace(",", "")) - Val(TextBox1.Text))) < 0 Then
                    ChangeLabel.Text = (Val(TotalDueLabel.Text.Replace("$", "").Replace(",", "")) - Val(TextBox1.Text)).ToString("c2")
                Else
                    ChangeLabel.Text = 0
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim ask As DialogResult = MsgBox("Are you sure you wan to remove item?", MsgBoxStyle.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            If OmaxGridview1.SelectedRows.Count > 1 Then
                RemoveSelectedProducts(OmaxGridview1.GetSelectedRowCellValues("SaleID"))
            Else
                OmaxGridview1.DataSource = RemoveItem(Val(OmaxGridview1.GetSelectedRowCellDisplayText("SaleID")), _
                                                                        CustomerIDs, Val(OmaxGridview1.GetSelectedRowCellDisplayText("Qty")), _
                                                                        Val(OmaxGridview1.GetSelectedRowCellDisplayText("ProductID")))
            End If
            RefreshTotal()
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        Try
            OmaxGridview2.DataSource = FilterProducts(TextBox2.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            Button5.ShowPopUp()
            Try
                OmaxGridview3.DataSource = GetAllCustomer()
                OmaxGridview3.BestFitAllRowsAndColumns()
            Catch ex As Exception

            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged
        Try
            OmaxGridview3.DataSource = FilterCustomers(TextBox3.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click, OmaxGridview3.DoubleClick
        Try
            LoadCustomer(OmaxGridview3.GetSelectedRowCellDisplayText("CustomerID"))
            Button5.ClosePopUp()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub OmaxGridview2_KeyDown(sender As Object, e As KeyEventArgs) Handles OmaxGridview2.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Button3.PerformClick()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Button1.PerformClick()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub OmaxGridview3_KeyDown(sender As Object, e As KeyEventArgs) Handles OmaxGridview3.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Button6.PerformClick()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class