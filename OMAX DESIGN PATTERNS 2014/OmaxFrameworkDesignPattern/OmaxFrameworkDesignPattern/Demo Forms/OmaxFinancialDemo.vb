﻿Imports OmaxFrameworkDesignPattern.OmaxFramework
Imports OmaxFrameworkDesignPattern.OmaxFramework.Financial

Public Class OmaxFinancialDemo

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Label1.Text = AutoLoanPayment(Val(InterestRate.Text), Val(LoanPeriod.Text), Val(LoanAmount.Text)).ToString("c2")
            Dim AutoLoanList As New OmaxFramework.Financial
            AutoLoanList.AutoLoanPaymentToList(Val(InterestRate.Text), Val(LoanPeriod.Text), Val(LoanAmount.Text))
            OmaxGridview1.DataSource = AutoLoanResultSet
            Label7.Text = AutoLoanList.TotalInterest.ToString("c2")
            Label8.Text = AutoLoanList.TotalPayment.ToString("c2")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        OmaxGridview1.ShowPrintPreview()
    End Sub
End Class
