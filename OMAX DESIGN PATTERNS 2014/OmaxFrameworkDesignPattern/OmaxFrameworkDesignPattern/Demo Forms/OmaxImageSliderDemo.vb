﻿Imports OmaxFrameworkDesignPattern.OmaxFramework.OmaxData
Public Class OmaxImageSliderDemo

    Private Sub OmaxImageSliderDemo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OmaxPhotoSlider1.LoadSliderImages()
        Catch ex As Exception

        End Try
    End Sub
    Private IdleCounter As Integer
    Private IsIdle As Boolean
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles IdleChecker.Tick
        Try
            If IsIdle = True Then
                IdleCounter += 1
            Else
                IdleCounter = 0
            End If

            If IdleCounter >= 5 And IsIdle = True Then
                Panel1.Visible = False
            Else
                Panel1.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub

   
    Private Sub InitiateNotIdle(sender As Object, e As EventArgs) Handles OmaxPhotoSlider1.MouseEnter, Panel1.MouseEnter, Button1.MouseEnter, Button2.MouseEnter, Button3.MouseEnter, Button4.MouseEnter
        IsIdle = False
    End Sub

    Private Sub InitiateIdle(sender As Object, e As EventArgs) Handles OmaxPhotoSlider1.MouseLeave, Panel1.MouseLeave, Button1.MouseLeave, Button2.MouseLeave, Button3.MouseLeave, Button4.MouseLeave
        IsIdle = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        OmaxPhotoSlider1.SlidePrevious()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        OmaxPhotoSlider1.SlideNext()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        OmaxPhotoSlider1.StopSlide()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        OmaxPhotoSlider1.StartSlide()
    End Sub
End Class